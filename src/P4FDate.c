#include "P4FDate.h"

void SetCurrentDate(P4FDATE* P4FDate, struct tm *t)
{
	//RemoveAndDeIntDate(P4FDate);
	static char txtDate[] = "00 00";
	strftime(txtDate, sizeof(txtDate), "%m/%d", t);
	text_layer_set_text(P4FDate->txtlyrDate, txtDate);
	P4FDate->State.bSetTxtLyrDate = true;
	//P4FDate->iCurrentDay =  t->tm_mday;
}

void SetCurrentDateWord(P4FDATE* P4FDate, struct tm *t)
{	
	//RemoveAndDeIntDateWord(P4FDate);
	static char txtDateWord[] = " Xxx";
	strftime(txtDateWord, sizeof(txtDateWord), " %a", t);

	//uppercase it, should be by itself, but fuck it.
	for (int i = 0; txtDateWord[i] != 0; i++) 
	{
		if (txtDateWord[i] >= 'a' && txtDateWord[i] <= 'z')
		{
			txtDateWord[i] -= 0x20;
		}
	}

	text_layer_set_text(P4FDate->txtlyrDateWord, txtDateWord);	
	P4FDate->State.bSetTxtLyrDateWord = true;
}

void SetCurrentDateAndDateWord(P4FDATE* P4FDate, struct tm *t)
{
	////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Setting Current Date and Word");
	SetCurrentDate(P4FDate, t);
	SetCurrentDateWord(P4FDate,t);
	P4FDate->iCurrentDay =  t->tm_mday;
}

void RemoveAndDeIntDate(P4FDATE* P4FDate)
{
	if (P4FDate->State.bSetTxtLyrDate)
	{
		layer_remove_from_parent(text_layer_get_layer(P4FDate->txtlyrDate));
		text_layer_destroy(P4FDate->txtlyrDate);
		P4FDate->State.bSetTxtLyrDate = false;
	}
		
	if (P4FDate->State.bAniSlideLeftDayExists)
	{
		property_animation_destroy(P4FDate->aniSlideLeftDay);
		P4FDate->State.bAniSlideLeftDayExists = false;
		////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "destroyed left day ani");
	}
}

void RemoveAndDeIntDateWord(P4FDATE* P4FDate)
{
	if (P4FDate->State.bSetTxtLyrDateWord)
	{
		layer_remove_from_parent(text_layer_get_layer(P4FDate->txtlyrDateWord));
		text_layer_destroy(P4FDate->txtlyrDateWord);
		
		P4FDate->State.bSetTxtLyrDateWord = false;
	}
	
	if (P4FDate->State.bAniSlideLeftWordExists)
	{
		property_animation_destroy(P4FDate->aniSlideLeftWord);
		P4FDate->State.bAniSlideLeftWordExists = false;
	}
}


void RemoveAndDeIntAllDate(P4FDATE* P4FDate)
{
	RemoveAndDeIntDate(P4FDate);
	RemoveAndDeIntDateWord(P4FDate);
}

void P4FDATE_DTDT_INIT(P4FDATE* P4FDate, int icon, bool bInputChangeDay, Layer* layer)
{	
	if (bInputChangeDay)
	{
		switch(icon)
		{
			case 0:
				P4FDate->fraDateFrame = DT_FRA_DAY_1_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_1_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_0_WORD;
				P4FDate->fraDateEnd	  = DT_FRA_DAY_0_DAY;
				break;
			case 1:
				P4FDate->fraDateFrame = DT_FRA_DAY_2_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_2_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_1_WORD;
				P4FDate->fraDateEnd	  = DT_FRA_DAY_1_DAY;
				break;
			case 2:
				P4FDate->fraDateFrame = DT_FRA_DAY_3_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_3_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_2_WORD;
				P4FDate->fraDateEnd	  = DT_FRA_DAY_2_DAY;
				break;
			case 3:
				P4FDate->fraDateFrame = DT_FRA_DAY_4_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_4_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_3_WORD;
				P4FDate->fraDateEnd	  = DT_FRA_DAY_3_DAY;
				break;
		}
	}
	else
	{
		switch(icon)
		{
			case 0:
				P4FDate->fraDateFrame = DT_FRA_DAY_0_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_0_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_0_WORD;
				P4FDate->fraDateEnd	  = DT_FRA_DAY_0_DAY;
				break;
			case 1:
				P4FDate->fraDateFrame = DT_FRA_DAY_1_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_1_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_1_WORD;
				P4FDate->fraDateEnd	  = DT_FRA_DAY_1_DAY;
				break;
			case 2:
				P4FDate->fraDateFrame = DT_FRA_DAY_2_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_2_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_2_WORD;
				P4FDate->fraDateEnd	  = DT_FRA_DAY_2_DAY;
				break;
			case 3:
				P4FDate->fraDateFrame = DT_FRA_DAY_3_DAY;
				P4FDate->fraWordFrame = DT_FRA_DAY_3_WORD;
				P4FDate->fraWordEnd   = DT_FRA_DAY_3_WORD;
				P4FDate->fraDateEnd	  = DT_FRA_DAY_3_DAY;
				break;
		}
	}	
		
	P4FDate->txtlyrDateWord = text_layer_create(P4FDate->fraWordFrame);
	text_layer_set_text_color(P4FDate->txtlyrDateWord, GColorWhite);
	text_layer_set_background_color(P4FDate->txtlyrDateWord, GColorClear);
	text_layer_set_text_alignment(P4FDate->txtlyrDateWord, GTextAlignmentCenter);
	layer_insert_below_sibling(text_layer_get_layer(P4FDate->txtlyrDateWord),layer);
		
	P4FDate->txtlyrDate = text_layer_create(P4FDate->fraDateFrame);
	text_layer_set_text_color(P4FDate->txtlyrDate, GColorWhite);
	text_layer_set_background_color(P4FDate->txtlyrDate, GColorClear);
	text_layer_set_text_alignment(P4FDate->txtlyrDate, GTextAlignmentCenter);
	layer_insert_below_sibling(text_layer_get_layer(P4FDate->txtlyrDate),layer);
	
	P4FDate->iPreviousDay = 88;
	P4FDate->iCurrentDay = 99;
	P4FDate->State.bInitialized = true;
}

void P4FDATE_INIT(P4FDATE* P4FDate, Layer* layer)
{
	
	P4FDate->fraDateFrame = WATCH_FRA_DATE;
	P4FDate->fraWordFrame = WATCH_FRA_WORD_DATE;
	
	P4FDate->txtlyrDate = text_layer_create(P4FDate->fraDateFrame);
	text_layer_set_text_color(P4FDate->txtlyrDate, GColorWhite);
	text_layer_set_background_color(P4FDate->txtlyrDate, GColorClear);
	layer_add_child(layer, text_layer_get_layer(P4FDate->txtlyrDate));

	P4FDate->txtlyrDateWord = text_layer_create(P4FDate->fraWordFrame);
	text_layer_set_text_color(P4FDate->txtlyrDateWord, GColorWhite);
	text_layer_set_background_color(P4FDate->txtlyrDateWord, GColorClear);
	layer_add_child(layer, text_layer_get_layer(P4FDate->txtlyrDateWord));
	//Need to change this for the P4GWatchface, perhaps add in a bool to default the days or not?
	P4FDate->iPreviousDay = 88;
	P4FDate->iCurrentDay = 99;
	P4FDate->State.bInitialized = true;
}

void GetSetPreviousDayMonthYear(struct tm* tInputTime)
{
	int iLastDayOfMonth[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
	
	//offset can be + or -, it will act the same 
	tInputTime->tm_mday = tInputTime->tm_mday -1;

	if ((tInputTime->tm_mon == 0 && tInputTime->tm_mday > 30) || (tInputTime->tm_mon == 1 && tInputTime->tm_mday >27) || (tInputTime->tm_mon == 2 && tInputTime->tm_mday < 3))
	{
		//calc leap year
		if((tInputTime->tm_year%400==0) || (tInputTime->tm_year%4==0))
			iLastDayOfMonth[1] = 29;
		else
			iLastDayOfMonth[1] = 28;
	}

	//now for the offset Days part
	if (tInputTime->tm_mday > iLastDayOfMonth[tInputTime->tm_mon])
	{
		////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "day > last day of month");
		tInputTime->tm_mday -= iLastDayOfMonth[tInputTime->tm_mon];
		if (tInputTime->tm_mon != 0)
			tInputTime->tm_mon -=1;
		else
		{
			tInputTime->tm_mon 	 = 11;
			////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "year pre -1 == %d", tInputTime->tm_year);
			tInputTime->tm_year -= 1;
		}
			
	}
	else if (tInputTime->tm_mday <= 0)
	{
		////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "mday <= 0");
		if (tInputTime->tm_mon != 0)
		{
			////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "mon pre -1 = %d", tInputTime->tm_mon);
			tInputTime->tm_mon -=1;
		}
		else
		{
			////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "mon == 11");
			tInputTime->tm_mon = 11;
			tInputTime->tm_year -= 1;
			////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "year-1 is %d", tInputTime->tm_year);
		}
		tInputTime->tm_mday = iLastDayOfMonth[tInputTime->tm_mon] - tInputTime->tm_mday;
		////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "tm_mday after subs is %d", tInputTime->tm_mday);
	}
}


int OffsetDayOfWeekChar2Int(char* dayOfWeek, int iOffset)
{				
	int iOffsetDayOfWeek = 0;

	if (dayOfWeek[0] ==  '1')
		iOffsetDayOfWeek = 1;
	else if (dayOfWeek[0] ==  '2')
		iOffsetDayOfWeek = 2;
	else if (dayOfWeek[0] ==  '3')
		iOffsetDayOfWeek = 3;
	else if (dayOfWeek[0] == '4')
		iOffsetDayOfWeek = 4;
	else if (dayOfWeek[0] ==  '5')
		iOffsetDayOfWeek = 5;
	else if (dayOfWeek[0] == '6')
		iOffsetDayOfWeek = 6;
	else if (dayOfWeek[0] ==  '7')
		iOffsetDayOfWeek = 7;
		
	return iOffsetDayOfWeek + iOffset;
}

int SetOffsetDays(P4FDATE* P4FDate,int iOffset)
{
	int iLastDayOfMonth[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
	
	time_t tTime = time(NULL);
	struct tm* tInputTime = localtime(&tTime);

	static char day[] = " 0";
	static char dayOfWeek[] = "0";
	
	//offset can be + or -, it will act the same 
	tInputTime->tm_mday = tInputTime->tm_mday + iOffset;
	
	if ((tInputTime->tm_mon == 0 && tInputTime->tm_mday > 30) || (tInputTime->tm_mon == 1 && tInputTime->tm_mday >27) || (tInputTime->tm_mon == 2 && tInputTime->tm_mday < 3))
	{
		//calc leap year
		if((tInputTime->tm_year%400==0) || (tInputTime->tm_year%4==0))
			iLastDayOfMonth[1] = 29;
		else
			iLastDayOfMonth[1] = 28;
	}

	//now for the offset Days part
	if (tInputTime->tm_mday > iLastDayOfMonth[tInputTime->tm_mon])
	{
		tInputTime->tm_mday -= iLastDayOfMonth[tInputTime->tm_mon];
		////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "mday> last day of month  = %d", tInputTime->tm_mday);
	}
	else if (tInputTime->tm_mday <= 0)
	{
		if (tInputTime->tm_mon != 0)
		{
			////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "mon pre -1 = %d", tInputTime->tm_mon);
			tInputTime->tm_mon -=1;
		}
		else
		{
			////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "mon == 11");
			tInputTime->tm_mon = 11;
			tInputTime->tm_year -= 1;
			////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "year-1 is %d", tInputTime->tm_year);
		}
		////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "tm_mday 29 before sub  = %d", tInputTime->tm_mday);
		tInputTime->tm_mday += iLastDayOfMonth[tInputTime->tm_mon];// - tInputTime->tm_mday;
		////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "mday<0  = %d", tInputTime->tm_mday);
	}
		
	strftime(day, sizeof(day), "%d",tInputTime);
	strcpy(P4FDate->Date,(char*)day);
	text_layer_set_text(P4FDate->txtlyrDate, P4FDate->Date);
	P4FDate->State.bSetTxtLyrDate = true;
	////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Date = %s", P4FDate->Date);
	
	strftime(dayOfWeek, sizeof(dayOfWeek), "%u",tInputTime);
	////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "dayofweek = %s",dayOfWeek);
	int iReturn = OffsetDayOfWeekChar2Int(dayOfWeek, iOffset);
	return iReturn;
}

void SetOffsetWordDayOfWeek(P4FDATE* P4FDate, int iOffset)
{	
	int iOffsetDayOfWeek;
	iOffsetDayOfWeek = SetOffsetDays(P4FDate,iOffset);
	
	while (!(iOffsetDayOfWeek >= 1 && iOffsetDayOfWeek <= 7))
	{
		if (iOffsetDayOfWeek < 1)
			iOffsetDayOfWeek += 7;
		else if (iOffsetDayOfWeek > 7)
			iOffsetDayOfWeek -= 7;
	}

	switch (iOffsetDayOfWeek)
	{
		case 1:
			strcpy(P4FDate->wordDay,(char*)"MON");
			break;
		case 2:
			strcpy(P4FDate->wordDay,(char*)"TUE");
			break;
		case 3:
			strcpy(P4FDate->wordDay,(char*)"WED");
			break;
		case 4:
			strcpy(P4FDate->wordDay,(char*)"THU");
			break;
		case 5:
			strcpy(P4FDate->wordDay,(char*)"FRI");
			break;
		case 6:
			strcpy(P4FDate->wordDay,(char*)"SAT");
			break;	
		case 7:
			strcpy(P4FDate->wordDay,(char*)"SUN");
			break;
	}

	text_layer_set_text(P4FDate->txtlyrDateWord, P4FDate->wordDay);	
	P4FDate->State.bSetTxtLyrDateWord = true;	
}

void P4FDATE_DEINIT(P4FDATE* P4FDate)
{
	P4FDate->State.bInitialized = false;
}
