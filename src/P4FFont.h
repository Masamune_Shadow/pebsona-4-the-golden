#include "P4FDefinitions.h"
	
typedef struct
{
	bool bInitialized;
	bool bExistsFontDate;
	bool bExistsFontAbbr;
	bool bExistsFontTime;
	bool bSetFontDate;
	bool bSetFontAbbr;
	bool bSetFontTime;
} DateFontStateVars;

typedef struct
{
	bool bInitialized;
	bool bExistsFontMonth;
	bool bExistsFontYear;
	bool bSetFontMonth;
	bool bSetFontYear;
} DTDTFontStateVars;

typedef struct
{
	bool bInitialized;
	bool bExistsFontDTDT;
	bool bExistsFontDate;
	bool bSetFontDate;
	bool bSetFontDTDT;

} FontStateVars;

	
typedef struct
{
	GFont fontDate;
	GFont fontAbbr;
	GFont fontTime;
	ResHandle resDate;
	ResHandle resAbbr;
	ResHandle resTime;
	DateFontStateVars State;
} P4FDATEFONTS;

typedef struct
{
	GFont fontMonth; 
	GFont fontYear;	
	ResHandle resMonth;	
	ResHandle resYear;	
	DTDTFontStateVars State;
} P4FDTDTFONTS;

typedef struct
{
	P4FDATEFONTS P4FDateFonts;
	P4FDTDTFONTS P4FDTDTFonts;
	FontStateVars State;
} P4FFONTS;

extern P4FDATEFONTS P4FDateFonts;
extern P4FDTDTFONTS P4FDTDTFonts;
extern P4FFONTS P4FDTDTRANSFonts;
extern P4FFONTS P4FFonts;
	
void InitAllFonts(P4FFONTS* P4FFonts);
void InitDateFonts(P4FFONTS* P4FFonts);
void InitDTDTFonts(P4FFONTS* P4FFonts);
void InternalInitDateFonts(P4FDATEFONTS* P4FDateFonts);
void InternalInitDTDTFonts(P4FDTDTFONTS* P4FDTDTFonts);
void P4FDATEFONTS_DEINIT(P4FFONTS* P4FFonts);
void P4FDTDTFONTS_DEINIT(P4FFONTS* P4FFonts);
void P4FFONTS_DEINIT_ALL(P4FFONTS* P4FFonts);

void RemoveAndDeIntAllFonts(P4FFONTS* P4FFont);
void RemoveAndDeIntDateFonts(P4FDATEFONTS* P4FDateFonts);
void RemoveAndDeIntDTDTFonts(P4FDTDTFONTS* P4FDTDTFonts);
