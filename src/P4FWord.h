#include "P4FDefinitions.h"

typedef struct 
{
	bool bInitialized;
	
	bool bExistsImgBLACK;
	bool bExistsLyrBLACK;
	bool bExistsImgWHITE;
	bool bExistsLyrWHITE;
	
	bool bSetImgBLACK;
	bool bSetLyrBLACK;
	bool bSetImgWHITE;
	bool bSetLyrWHITE;
	
	bool bAniSlideLeftStart_BLACKExists;
	bool bAniSlideLeftEnd_BLACKExists;
	bool bAniSlideLeftStart_WHITEExists;
	bool bAniSlideLeftEnd_WHITEExists;
	
} WordStateVars;

typedef struct 
{
	BitmapLayer* bmplyrWord_WHITE;
	BitmapLayer* bmplyrWord_BLACK;
	GBitmap* imgWord_WHITE;
	GBitmap* imgWord_BLACK;
	
	/*
		static GBitmap* imgWord_WHITE[9];
		static GBitmap* imgWord_BLACK[9];
	*/
	
	int iCurrentWord;
	int iPreviousWord;
	bool bWordSet;
	GRect fraWordFrame;
	WordStateVars State;
	
	GRect fraWordStart;
	GRect fraWordHold;
	GRect fraWordEnd;
	
	PropertyAnimation* aniSlideLeftStart_BLACK;
	PropertyAnimation* aniSlideLeftHold_BLACK;
	PropertyAnimation* aniSlideLeftEnd_BLACK;
	PropertyAnimation* aniSlideLeftStart_WHITE;
	PropertyAnimation* aniSlideLeftHold_WHITE;
	PropertyAnimation* aniSlideLeftEnd_WHITE;
} P4FWORD;
	
extern P4FWORD P4FWord;
//extern P4FWORD P4FWTWTWord;
		
void P4FWORD_INIT(P4FWORD* P4FWord);
void P4FWORD_DEINIT(P4FWORD* P4FWord);
void GetSetCurrentWord(P4FWORD* P4FWord,Layer* layer, bool bIsWTWT);
void RemoveAndDeIntWord(P4FWORD* P4FWord);
void GetWordInteger(P4FWORD* P4FWord, int iCurrentHour);
void SetWordImages(P4FWORD* P4FWord, Layer* layer);
void SetWordImageBLACK(P4FWORD* P4FWord, Layer* layer);
void SetWordImageWHITE(P4FWORD* P4FWord, Layer* layer);
void SetCurrentWordImage(P4FWORD* P4FWord);
bool ChangeWordCheck(P4FWORD* P4FWord);
