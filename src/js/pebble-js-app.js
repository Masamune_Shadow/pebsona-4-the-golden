
// config.html will be included by build process, see build/src/js/pebble-js-app.js
//var config_html; 
// This gets replaced during build
var updateInProgress = false;
var updatingWeather = false;
var updatingSettings = false;

var config_html = '__CONFIG_HTML__';

function mix(destination, source) {
	for (var key in source) {
		if (source.hasOwnProperty(key)) {
			destination[key] = source[key];
		}
	}

	return destination;
}

var config = mix(Object.create(null), {
	//UseTVColor: 		0,
	UseInvertedAxis: 	0,
	UseTemperature: 	0,
	UseCelsius: 		0,
	//UseInvertedColor:	0,
});

var CurrentTemperature = 999;

function iconFromWeatherId(weatherId) 
{
  if (weatherId < 300) {
	return 5;
  } else if (weatherId < 600) {
    return 2;
  } else if (weatherId < 700) {
    return 3;
  } else if (weatherId > 800) {
    return 1;
  } else {
    return 0;
  }
}

function fetchWeather() {
  var response;
  var latlon;
  var req = new XMLHttpRequest();
  navigator.geolocation.getCurrentPosition(success);
  function success(position) 
  {
	// Retrive the cache
    //var cache = localStorage.iconCache && JSON.parse(localStorage.iconCache);
    var d = new Date();
    //var iReturnIcon1 = 7;
    //var iReturnIcon2 = 7;
    var iReturnIconDefaultValue = 7;
    
    icon = isNaN(getStorageInt("icon")) ? iReturnIconDefaultValue:getStorageInt("icon");
    var iReturnIcon3 = isNaN(getStorageInt("iReturnIcon3")) ? iReturnIconDefaultValue:getStorageInt("iReturnIcon3");
    var iReturnIcon4 = isNaN(getStorageInt("iReturnIcon4")) ? iReturnIconDefaultValue:getStorageInt("iReturnIcon4");
    var iReturnIcon5 = isNaN(getStorageInt("iReturnIcon5")) ? iReturnIconDefaultValue:getStorageInt("iReturnIcon5");
	var iReturnIcon6 = isNaN(getStorageInt("iReturnIcon6")) ? iReturnIconDefaultValue:getStorageInt("iReturnIcon6");
	var iReturnIcon7 = isNaN(getStorageInt("iReturnIcon7")) ? iReturnIconDefaultValue:getStorageInt("iReturnIcon7");
	var iReturnIcon8 = isNaN(getStorageInt("iReturnIcon8")) ? iReturnIconDefaultValue:getStorageInt("iReturnIcon8");
	CurrentTemperature = isNaN(getStorageInt("CurrentTemperature")) ? iReturnIconDefaultValue:getStorageInt("CurrentTemperature");
	
	/*
	var iReturnBoolDefaultValue = 7;
	var iUseTVColors 		= iReturnBoolDefaultValue;
	var iUseInvertedAxis 	= iReturnBoolDefaultValue;
	*/
	// If the cache is newer than 30 min hours, use the cache
			
		var latitude  = position.coords.latitude;
		var longitude = position.coords.longitude;

		var latlon = 'lat=' + latitude + '&lon=' + longitude;
		//console.log(latlon);
		//console.log("http://api.openweathermap.org/data/2.5/forecast?" + latlon + "&APPID=ac3eeed2e811a06c13859db0b7d18d05&cnt=12");
		  req.open('GET', 'http://api.openweathermap.org/data/2.5/forecast?' + latlon + '&APPID=ac3eeed2e811a06c13859db0b7d18d05&cnt=12', true);
		  req.onload = function(e) {
			if (req.readyState == 4) {
			  if(req.status == 200) {
				response = JSON.parse(req.responseText);  
				var icon;
				if (response && response.list && response.list.length > 0) {
				  //var weatherResult = response.list[0];
				  var iIterate = response.cnt;
				  var iStartDay = new Date();
				  var iTomorrowStart = new Date();
				  
				  iStartDay.setTime(response.list[0].dt * 1000);
				  
				  //TEMPERATURE
				  //var iReturnTemperature = 200;
				  //iReturnTemperature = Math.round(response.list[0].main.temp - 273.15);
				  CurrentTemperature = Math.round(response.list[0].main.temp - 273.15);
				  //console.log("iReturnTempurature " + CurrentTemperature);
				  for (i = 0; i < iIterate; i++)
				  {
					  var resultDate = new Date();
					  //should have this
					  resultDate.setTime(response.list[i].dt * 1000);
					   //console.log("result day + dt = " + resultDate.getDay() + "  " + resultDate.getTime());
					 // console.log ("Iterator I = " + i + " dt =" + response.list[i].dt);
					  if ( i == 0)
					  {
						  icon = iconFromWeatherId(response.list[i].weather[0].id);
						  iReturnIcon3 = iconFromWeatherId(response.list[i].weather[0].id);
						  //return5 (today morn)
						  if (resultDate.getHours() <= 15)
						  {
								iReturnIcon3 = iconFromWeatherId(response.list[i].weather[0].id);
						  }
						  
						  if (resultDate.getHours() > 15 && resultDate.getHours() < 24 && iReturnIcon6 == iReturnIconDefaultValue)
						  {
								iReturnIcon4 = iconFromWeatherId(response.list[i].weather[0].id);
						  }
					  }
					  
					  if (iStartDay.getDay() == resultDate.getDay() && resultDate.getHours() >= 15 && resultDate.getHours() <= 24 && iReturnIcon4 == iReturnIconDefaultValue)
					  {
							iReturnIcon4 = iconFromWeatherId(response.list[i].weather[0].id);
					  }


					  if (iStartDay.getDay() != resultDate.getDay() && resultDate.getHours() >= 5 && resultDate.getHours() <= 12 && iReturnIcon5 == iReturnIconDefaultValue) //resultDate.getHours() == 6
					  { 
						  iReturnIcon5 = iconFromWeatherId(response.list[i].weather[0].id);
						  //console.log ("hit 5 on iteration " + i);
					  }
					 					  
					  if (iStartDay.getDay() != resultDate.getDay() && resultDate.getHours() >= 15 && resultDate.getHours() <= 24 && iReturnIcon6 == iReturnIconDefaultValue) //resultDate.getHours() == 18
					  {
						iReturnIcon6 = iconFromWeatherId(response.list[i].weather[0].id); 
						//console.log ("hit 6 on iteration " + i);
						iTomorrowStart.setTime(resultDate.getTime());
					  }
					  
					  if (iTomorrowStart.getDay() != resultDate.getDay() && resultDate.getHours() >= 5 && resultDate.getHours() <= 12 && iReturnIcon7 == iReturnIconDefaultValue) //resultDate.getHours() == 6
					  { 
						 // console.log ("hit 7 on iteration " + i);
						  iReturnIcon7 = iconFromWeatherId(response.list[i].weather[0].id);
					  }
					  
					  if (iTomorrowStart.getDay() != resultDate.getDay() && resultDate.getHours() >= 15 && resultDate.getHours() <= 24 && iReturnIcon8 == iReturnIconDefaultValue) //resultDate.getHours() == 18
					  {
						if (i > iReturnIconDefaultValue)
						{
							//console.log ("hit 8 on iteration " + i);
							iReturnIcon8 = iconFromWeatherId(response.list[i].weather[0].id); 
							break;
						}
					  }
					  
				  }
					//console.log("config usecelsius " + config.UseCelsius);
				    if (config.UseCelsius == 0)
					{
						CurrentTemperature = Math.round((((CurrentTemperature * 9)/5)+32));
					}	
					//Dummy var used for temp, won't show if = -128
					
					if (config.UseTemperature == 0)
					{
						CurrentTemperature = -50;
					}
						
					
					localStorage.setItem("Icon", icon);
					localStorage.setItem("iReturnIcon3", iReturnIcon3);
					localStorage.setItem("iReturnIcon4", iReturnIcon4);
					localStorage.setItem("iReturnIcon5", iReturnIcon5);
					localStorage.setItem("iReturnIcon6", iReturnIcon6);
					localStorage.setItem("iReturnIcon7", iReturnIcon7);
					localStorage.setItem("iReturnIcon8", iReturnIcon8);
					
					
					/*
					
						console.log ("Icon " + icon + 
									  " iReturnIcon3 " + iReturnIcon3 + 
									  " iReturnIcon4 " + iReturnIcon4 + 
									  " iReturnIcon5 " + iReturnIcon5 + 
									  " iReturnIcon6 " + iReturnIcon6 + 
									  " iReturnIcon7 " + iReturnIcon7 + 
									  " iReturnIcon8 " + iReturnIcon8 +							
									  " UseTVColor " + config.UseTVColor +
									  " UseInvertedAxis " + config.UseInvertedAxis +
									  " UseTemperature " + config.UseTemperature +
									  " Temp " + CurrentTemperature + 
									  " UseInvertedColor " + config.UseInvertedColor);
						*/			  
									  
						Pebble.sendAppMessage({"icon": icon,
												"+0Mor":iReturnIcon3,
												"+0Eve":iReturnIcon4,
												"+1Mor":iReturnIcon5,
												"+1Eve":iReturnIcon6,
												"+2Mor":iReturnIcon7,
												"+2Eve":iReturnIcon8,
												//"UseTVColor":config.UseTVColor,
												"UseInvertedAxis":config.UseInvertedAxis,
												"Temp" : CurrentTemperature
												//"UseInvertedColor":config.UseInvertedColor
												});
												//"UseTemperature":config.UseTemperature,
												
					//}	
				
				  updatingWeather = false;
				  updateInProgress = false;

			  } else {
				console.log("Error");
			  }
			}
		  }
		}
	req.send(null);
   }
}


function locationSuccess(pos) {
  fetchWeather();
}

function locationError(err) {
  //console.warn('location error (' + err.code + '): ' + err.message);
	console.log ('Error');
}



function updateWeather()
{
	if (!updateInProgress)
	{
		updateInProgress=true;
		updatingWeather = true;
		var locationOptions = { "timeout": 15000, "maximumAge": 60000  }; 
		window.navigator.geolocation.getCurrentPosition(locationSuccess, locationError, locationOptions);
	}
	else
	{
		console.log("Request already in progress");
	}
	
}

function getStorageInt(key) {
	var value = localStorage.getItem(key);
	if (value != null) {
		return parseInt(value, 10);
	}
	return value;
}

Pebble.addEventListener("ready",
                        function(e) {
                        //console.log("Ready!");
						for (var key in config) 
						{
							var value = getStorageInt(key);
							//console.log("Key " + key + " Value " + value);
							if (value != null) 
							{
								config[key] = value;
								//console.log("Key " + config + " Value " + config[key]);
							}
						}
                        });

Pebble.addEventListener("appmessage",
                        function(e) {
							if (e.payload.config) {
								//Pebble.sendAppMessage(config);
							}
						  var currentdt = new Date();
						  //console.log("Called @ " + currentdt);
						  updateWeather();
                        });
                        

Pebble.addEventListener('showConfiguration', function (e) {
	console.log('showConfigurationEvent');
	updatingSettings = true;
	var html = config_html.replace('__CONFIG__', JSON.stringify(config));
	//console.log("HTML is " + 'data:text/html,' + encodeURIComponent(html + '<!--.html'));
	Pebble.openURL('data:text/html,' + encodeURIComponent(html + '<!--.html'));
});

Pebble.addEventListener('webviewclosed', function (e) {
	var configuration;
	
	try {
		configuration = JSON.parse(e.response);			
		console.log(e.response);
	}
	catch (e) {
		console.log("Didn't work?");
		return;
	}
	//console.log("configuration" + configuration.UseTVColor);
	console.log (e.response);
	
	Pebble.sendAppMessage(mix(config, configuration));
	updateWeather();
	updatingSettings =false;
	for (var key in config) {
		localStorage.setItem(key, config[key]);
	}
	localStorage.setItem("CurrentTemperature",CurrentTemperature);
});


