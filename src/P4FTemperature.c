/*!!TEMPERATURE*/
#include "P4FTemperature.h"


void GetSetCurrentTemperature(P4FTEMPERATURE* P4FTemperature)
{
	if (P4FTemperature->State.bInitialized)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Getting Temp");
		if (fPersist)
		{
			P4FTemperature->iCurrentTemperature = GetPersistKey((uint32_t)P4FPKEY_CUR_TEMPERATURE);
		}
		else
		{
			P4FTemperature->iCurrentTemperature = -50;
		}
		SetCurrentTemperature(P4FTemperature);
	}
}


void SetCurrentTemperature(P4FTEMPERATURE* P4FTemperature)
{
	
	if (P4FTemperature->State.bInitialized)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "SetCurrent Temp");
		snprintf(P4FTemperature->strCurrentTemperature, sizeof(P4FTemperature->strCurrentTemperature), "%i%s", P4FTemperature->iCurrentTemperature, "°");
		text_layer_set_text(P4FTemperature->txtlyrP4FTemperature, P4FTemperature->strCurrentTemperature);
	}	
}

//void P4FTEMPERATURE_INIT(P4FTEMPERATURE* P4FTemperature)
void P4FTEMPERATURE_INIT(P4FTEMPERATURE* P4FTemperature, Layer* layer)
{
	if (!P4FTemperature->State.bInitialized)
	{
		
		 P4FTemperature->fraTemperatureFrame = WATCH_FRA_TEMPERATURE;
		
		P4FTemperature->txtlyrP4FTemperature = text_layer_create(P4FTemperature->fraTemperatureFrame);
		layer_add_child(layer, text_layer_get_layer(P4FTemperature->txtlyrP4FTemperature));
		
		text_layer_set_background_color(P4FTemperature->txtlyrP4FTemperature, GColorClear);
		text_layer_set_text_color(P4FTemperature->txtlyrP4FTemperature, GColorWhite);
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Display Temp");
		GetSetCurrentTemperature(P4FTemperature);
		//P4FTemperature->iCurrentTemperature = 200;
		//SetCurrentTemperature(P4FTemperature);
		text_layer_set_text_alignment(P4FTemperature->txtlyrP4FTemperature, GTextAlignmentRight);
		layer_mark_dirty(text_layer_get_layer(P4FTemperature->txtlyrP4FTemperature));
		
		//TEMPERATURE_IMAGES
		P4FTemperature->State.bInitialized = true;
	}
}

void P4FTEMPERATURE_DEINIT(P4FTEMPERATURE* P4FTemperature)
{
	if (P4FTemperature->State.bInitialized)
	{
		layer_remove_from_parent(text_layer_get_layer(P4FTemperature->txtlyrP4FTemperature));
		text_layer_destroy(P4FTemperature->txtlyrP4FTemperature);
		P4FTemperature->State.bInitialized = false;	
	}
}

