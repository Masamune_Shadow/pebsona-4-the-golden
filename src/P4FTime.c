#include "P4FTime.h"

void GetSetTime(P4FTIME* P4FTime)
{
	time_t tTime = time(NULL);
	struct tm* tmTime = localtime(&tTime);
	static char time_text[] = " 00:00"; // Needs to be static because it's used by the system later.
	char *time_format;
	 if (clock_is_24h_style()) {
		time_format = " %R";
	  } else {
		time_format = " %I:%M";
	  }

	  strftime(time_text, sizeof(time_text), time_format, tmTime);	 
	 text_layer_set_text(P4FTime->txtlyrP4FTime, (char*)time_text);
}

//void P4FTIME_INIT(P4FTIME* P4FTime)
void P4FTIME_INIT(P4FTIME* P4FTime, Layer* layer)
{
	if (!P4FTime->State.bInitialized)
	{
		
		 P4FTime->fraTimeFrame = WATCH_FRA_TIME;
		
		P4FTime->txtlyrP4FTime = text_layer_create(P4FTime->fraTimeFrame);
		layer_add_child(layer, text_layer_get_layer(P4FTime->txtlyrP4FTime));
		
		text_layer_set_background_color(P4FTime->txtlyrP4FTime, GColorClear);
		text_layer_set_text_color(P4FTime->txtlyrP4FTime, GColorWhite);
		time_t tTime = time(NULL);
		struct tm* tmTime = localtime(&tTime);
		static char time_text[] = "00:00"; // Needs to be static because it's used by the system later.
		char *time_format;
		 if (clock_is_24h_style()) {
			time_format = "%R";
		  } else {
			time_format = "%I:%M";
		  }

		  strftime(time_text, sizeof(time_text), time_format, tmTime);
		  
		 text_layer_set_text(P4FTime->txtlyrP4FTime, (char*)time_text);
		 text_layer_set_text_alignment(P4FTime->txtlyrP4FTime, GTextAlignmentLeft);
		layer_mark_dirty(text_layer_get_layer(P4FTime->txtlyrP4FTime));
		
		//TIME_IMAGES
		P4FTime->State.bInitialized = true;
		P4FTime->iPreviousHour = tmTime->tm_hour;
	}
}

void P4FTIME_DEINIT(P4FTIME* P4FTime)
{
	
	if (P4FTime->State.bInitialized)
	{
		layer_remove_from_parent(text_layer_get_layer(P4FTime->txtlyrP4FTime));
		text_layer_destroy(P4FTime->txtlyrP4FTime);
	}
	P4FTime->State.bInitialized = false;	
}
