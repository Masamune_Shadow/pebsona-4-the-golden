/*!!TEMPERATURE*/
#include "P4FDefinitions.h"

typedef struct 
{
	bool bInitialized;
	bool bAll;
} TemperatureStateVars;

typedef struct 
{		
	TextLayer* 		txtlyrP4FTemperature;
	
	
	//int iPreviousHour;
	//char Date[3];
	char strCurrentTemperature[5];
	int iCurrentTemperature;
	GRect fraTemperatureFrame;
	TemperatureStateVars State;

	//struct tm* tTime;
} P4FTEMPERATURE;

extern P4FTEMPERATURE P4FTemperature;

void P4FTEMPERATURE_INIT(P4FTEMPERATURE* P4FTemperature, Layer* layer);
void P4FTEMPERATURE_DEINIT(P4FTEMPERATURE* P4FTemperature);
void GetSetCurrentTemperature(P4FTEMPERATURE* P4FTemperature);
void SetCurrentTemperature(P4FTEMPERATURE* P4FTemperature);


