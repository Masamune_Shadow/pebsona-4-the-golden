//P4F = Pebsona 4: THE FRAMEWORK
//PFG = Pebsona 4: THE GOLDEN
#include "P4FDefinitions.h"
#include "P4FWord.h"
#include "P4FDate.h"
#include "P4FWeather.h"
#include "P4FTime.h"
#include "P4FFont.h"
#include "P4FTemperature.h"


typedef struct
{
	bool bExists;
	//bool bInvertExists;
	bool bInitializing;
	bool bInitialized;
	bool bSetBackground;
	bool bClosing;
} WatchfaceStateVars;
	
//put in weather
//static int iOurLatitude, iOurLongitude;

typedef struct
{
	BitmapLayer*	lyrP4GWatchface;
	GBitmap* imgBackground;
	GRect fraBackground;	

	WatchfaceStateVars State;	

	P4FTIME			P4FTime;
	P4FWORD 		P4FWord;
	P4FDATE 		P4FDate;
	P4FWEATHER		P4FWeather;
	P4FTEMPERATURE  P4FTemperature;	
	P4FFONTS		P4FFonts;
}P4GWATCHFACE;
	
void CreateImgBackground();
void CreateBackground();
void ChangeBackground(Window* window);
void ChangeTemperature();
void SetupWatchface(struct tm* t ,Window* window);
void RemoveAndDeInt(char* cToRnD);
void P4GWATCHFACE_DEINIT(char* cToRnD);
