#include "P4FDefinitions.h"

	
typedef struct
{
	bool bInitialized;
	bool bSetTxtLyrDate;
	bool bSetTxtLyrDateWord;

	bool bAniSlideLeftDayExists;
	bool bAniSlideLeftWordExists;
} DateStateVars;

typedef struct
{
	TextLayer* txtlyrDate;
	TextLayer* txtlyrDateWord;
	
	int iCurrentDay;
	int iPreviousDay;
	int iOffsetDay;
	
	GRect fraDateFrame;
	GRect fraWordFrame;
	DateStateVars State;
	char Date[3];
	char wordDay[3];
	//Animation
	GRect fraWordEnd;
	GRect fraDateEnd;
	
	PropertyAnimation* aniSlideLeftDay;
	PropertyAnimation* aniSlideLeftWord;
} P4FDATE;

extern P4FDATE P4FDate;

void P4FDATE_DTDT_INIT(P4FDATE* P4FDate, int icon, bool bInputChangeDay, Layer* layer);
void P4FDATE_INIT(P4FDATE* P4FDate,Layer* layer);
void P4FDATE_DEINIT(P4FDATE* P4FDate);
void SetCurrentDate(P4FDATE* P4FDate, struct tm *t);
void SetCurrentDateWord(P4FDATE* P4FDate, struct tm *t);
void SetCurrentDateAndDateWord(P4FDATE* P4FDate, struct tm *t);
void RemoveAndDeIntDate(P4FDATE* P4FDate);
void RemoveAndDeIntDateWord(P4FDATE* P4FDate);
void RemoveAndDeIntAllDate(P4FDATE* P4FDate);
int OffsetDayOfWeekChar2Int(char* dayOfWeek, int iOffset);
int SetOffsetDays(P4FDATE* P4FDate,int iOffset);
void SetOffsetWordDayOfWeek(P4FDATE* P4FDate, int iOffset);
void GetSetPreviousDayMonthYear(struct tm* tInputTime);

