#include "P4FDefinitions.h"

#ifndef WEATHER_LAYER_H
#define WEATHER_LAYER_H

typedef enum 
{
	WEATHER_SUN = 0,
	WEATHER_CLOUD,
	WEATHER_RAIN,
	WEATHER_SNOW,
	WEATHER_UNKNOWN,
	WEATHER_LIGHTNING
} WEATHERIMAGE;

typedef struct 
{
	bool bInitialized;
	
	bool bSplit;
	bool bSetImg;
	bool bSetLyr;
	bool bAniSlideLeftImgExists;
} WeatherStateVars;

typedef struct
{
	bool bSplit;
	bool bInitialized;
		
} SplitWeatherStateVars;

typedef struct 
{
	BitmapLayer* bmplyrWeather;
	GBitmap* imgWeather;
	WEATHERIMAGE CurrentWeather;
	WeatherStateVars State;
	bool bUseSquare;
	GRect fraWeatherFrame;
	GRect fraImgEnd;
	
	//Animation
	PropertyAnimation* aniSlideLeftImg;
} P4FWEATHER;

typedef struct 
{
	P4FWEATHER P4FSplitWeather_TOP;
	P4FWEATHER P4FSplitWeather_BOTTOM;
	SplitWeatherStateVars State;
} P4FSPLITWEATHER;

//Reg Def
void P4FWEATHER_DTDT_INIT(P4FWEATHER* P4FWeather, int iDay, Layer* layer);
void P4FWEATHER_INIT(P4FWEATHER* P4FWeather, Layer* layer);
void P4FWEATHER_DEINIT(P4FWEATHER* P4FWeather);
void GetSetWeatherImage(P4FWEATHER* P4FWeather, WEATHERIMAGE WeatherImage,Layer* layer);
void RemoveAndDeIntWeather(P4FWEATHER* P4FWeather);
//Split Def
void P4FSPLITWEATHER_DTDT_INIT(P4FSPLITWEATHER* P4FSplitWeather, int iDay, bool bInputChangeDay, Layer* layer);
void P4FSPLITWEATHER_DEINIT(P4FSPLITWEATHER* P4FSplitWeather);
void RemoveAndDeIntSplitWeather(P4FSPLITWEATHER* P4FSplitWeather);
void GetSetSplitWeatherImages(P4FSPLITWEATHER* P4FSPLITWeather,WEATHERIMAGE WeatherImage_TOP, WEATHERIMAGE WeatherImage_BOTTOM, Layer* layer);
void SetSplitWeatherImages(P4FSPLITWEATHER* P4FSplitWeather, WEATHERIMAGE WeatherImage_TOP,  WEATHERIMAGE WeatherImage_BOTTOM, Layer* layer);
void SetWeatherImageTOP(P4FWEATHER* P4FWeatherSplit_TOP, WEATHERIMAGE WeatherImage, Layer* layer);
void SetWeatherImageBOTTOM(P4FWEATHER* P4FWeatherSplit_BOTTOM, WEATHERIMAGE WeatherImage, Layer* layer);
#endif
