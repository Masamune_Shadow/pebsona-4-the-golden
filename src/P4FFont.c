#include "P4FFont.h"	

void InternalInitDateFonts(P4FDATEFONTS* P4FDateFonts)
{
	//if (!P4FDateFonts->State.bInitialized)
	//{
		P4FDateFonts->State.bInitialized = true;
		
		if (!P4FDateFonts->State.bExistsFontDate)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "EXISTS Date Date");
			P4FDateFonts->resDate = resource_get_handle(RESOURCE_ID_FONT_DAYS_26);
			P4FDateFonts->State.bExistsFontDate = true;
		}
		
		if (P4FDateFonts->State.bExistsFontDate && !P4FDateFonts->State.bSetFontDate)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Set Date Date");
			P4FDateFonts->fontDate = fonts_load_custom_font(P4FDateFonts->resDate);
			P4FDateFonts->State.bSetFontDate = true;
		}
		
		if (!P4FDateFonts->State.bExistsFontTime)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "EXISTS Date Date");
			P4FDateFonts->resTime = resource_get_handle(RESOURCE_ID_FONT_DAYS_20);
			P4FDateFonts->State.bExistsFontTime = true;
		}
		
		if (P4FDateFonts->State.bExistsFontTime && !P4FDateFonts->State.bSetFontTime)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Set Date Date");
			P4FDateFonts->fontTime = fonts_load_custom_font(P4FDateFonts->resTime);
			P4FDateFonts->State.bSetFontTime = true;
		}
		
		if (!P4FDateFonts->State.bExistsFontAbbr)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Exists Date Abbr");
			P4FDateFonts->resAbbr = resource_get_handle(RESOURCE_ID_FONT_DAYS_13);
			P4FDateFonts->State.bExistsFontAbbr = true;
		}
		
		if (P4FDateFonts->State.bExistsFontAbbr && !P4FDateFonts->State.bSetFontAbbr)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Set Date Abbr");
			P4FDateFonts->fontAbbr = fonts_load_custom_font(P4FDateFonts->resAbbr);			
			P4FDateFonts->State.bSetFontAbbr = true;
		}
	//}
}

void InternalInitDTDTFonts(P4FDTDTFONTS* P4FDTDTFonts)
{
	//if (!P4FDTDTFonts->State.bInitialized)
	//{
		P4FDTDTFonts->State.bInitialized = true;
		if (!P4FDTDTFonts->State.bExistsFontYear)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Exists DTDTYear");
			P4FDTDTFonts->resYear = resource_get_handle(RESOURCE_ID_FONT_DAYS_16);
			P4FDTDTFonts->State.bExistsFontYear = true;
		}
		
		if (P4FDTDTFonts->State.bExistsFontYear && !P4FDTDTFonts->State.bSetFontYear)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Set DTDTYear");
			P4FDTDTFonts->fontYear = fonts_load_custom_font(P4FDTDTFonts->resYear);
			P4FDTDTFonts->State.bSetFontYear = true;
		}
		
		if (!P4FDTDTFonts->State.bExistsFontMonth)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Exists DTDTMonth");
			P4FDTDTFonts->resMonth = resource_get_handle(RESOURCE_ID_FONT_DAYS_48);
			P4FDTDTFonts->State.bExistsFontMonth = true;
		}
		
		if (P4FDTDTFonts->State.bExistsFontMonth && !P4FDTDTFonts->State.bSetFontMonth)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Set DTDTMonth");
			P4FDTDTFonts->fontMonth = fonts_load_custom_font(P4FDTDTFonts->resMonth);
			P4FDTDTFonts->State.bSetFontMonth = true;
		}
	//}
}

void InitDateFonts(P4FFONTS* P4FFonts)
{
	if (!P4FFonts->State.bInitialized)
	{
		P4FFonts->State.bInitialized = true;
	}
	InternalInitDateFonts(&P4FFonts->P4FDateFonts);
}

void InitDTDTFonts(P4FFONTS* P4FFonts)
{
	if (!P4FFonts->State.bInitialized)
	{
		P4FFonts->State.bInitialized = true;
	}
	InternalInitDTDTFonts(&P4FFonts->P4FDTDTFonts);
}

void InitAllFonts(P4FFONTS* P4FFonts)
{
	if (!P4FFonts->State.bInitialized)
	{
		P4FFonts->State.bInitialized = true;
	}
	InitDateFonts(P4FFonts);
	InitDTDTFonts(P4FFonts);
}

void RemoveAndDeIntAllFonts(P4FFONTS* P4FFonts)
{
	if (P4FFonts->P4FDateFonts.State.bInitialized)
	{
		RemoveAndDeIntDateFonts(&P4FFonts->P4FDateFonts);
		P4FFonts->State.bExistsFontDate = false;
	}
	
	if (P4FFonts->P4FDTDTFonts.State.bInitialized)
	{
		RemoveAndDeIntDTDTFonts(&P4FFonts->P4FDTDTFonts);
		P4FFonts->State.bExistsFontDTDT = false;
	}	
}


void RemoveAndDeIntDateFonts(P4FDATEFONTS* P4FDateFonts)
{
	//if (P4FDateFonts->State.bInitialized)
	//{
		if (P4FDateFonts->State.bSetFontDate)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "unload Date Year");
			fonts_unload_custom_font(P4FDateFonts->fontDate);
			P4FDateFonts->State.bSetFontDate = false;
		}
		
		if (P4FDateFonts->State.bSetFontTime)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "unload Date Year");
			fonts_unload_custom_font(P4FDateFonts->fontTime);
			P4FDateFonts->State.bSetFontTime = false;
		}
		
		if (P4FDateFonts->State.bSetFontAbbr)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "unload Date Abbr");
			fonts_unload_custom_font(P4FDateFonts->fontAbbr);
			P4FDateFonts->State.bSetFontAbbr = false;
		}	
	//}
}

void RemoveAndDeIntDTDTFonts(P4FDTDTFONTS* P4FDTDTFonts)
{
	//if (P4FDTDTFonts->State.bInitialized)
	//{
		if (P4FDTDTFonts->State.bSetFontYear)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "unload DTDTYear");
			fonts_unload_custom_font(P4FDTDTFonts->fontYear);
			P4FDTDTFonts->State.bSetFontYear = false;
		}
		
		if (P4FDTDTFonts->State.bSetFontMonth)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "unload DTDTMonth");
			fonts_unload_custom_font(P4FDTDTFonts->fontMonth);
			P4FDTDTFonts->State.bSetFontMonth = false;
		}	
	//}
}

void P4FDATEFONTS_DEINIT(P4FFONTS* P4FFonts)
{
	P4FFonts->P4FDateFonts.State.bInitialized = false;
}

void P4FDTDTFONTS_DEINIT(P4FFONTS* P4FFonts)
{
	P4FFonts->P4FDTDTFonts.State.bInitialized = false;
}

void P4FFONTS_DEINIT_ALL(P4FFONTS* P4FFonts)
{
	P4FDATEFONTS_DEINIT(P4FFonts);
	P4FDTDTFONTS_DEINIT(P4FFonts);
	P4FFonts->State.bInitialized = false;
}
