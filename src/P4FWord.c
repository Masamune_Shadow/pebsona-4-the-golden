#include "P4FWord.h"

#define NUMBER_OF_IMAGES 9

const uint8_t WORD_IMAGES_WHITE[NUMBER_OF_IMAGES] = {
	RESOURCE_ID_IMAGE_TIME_MIDNIGHT_WHITE,
	RESOURCE_ID_IMAGE_TIME_EARLY_MORNING_WHITE,
	RESOURCE_ID_IMAGE_TIME_MORNING_WHITE,
	RESOURCE_ID_IMAGE_TIME_DAYTIME_WHITE,
	RESOURCE_ID_IMAGE_TIME_LUNCHTIME_WHITE,
	RESOURCE_ID_IMAGE_TIME_AFTERNOON_WHITE,
	RESOURCE_ID_IMAGE_TIME_EVENING_WHITE,
	RESOURCE_ID_IMAGE_TIME_NIGHT_WHITE,
	RESOURCE_ID_IMAGE_TIME_BLANK_WHITE,
};

const uint8_t WORD_IMAGES_BLACK[NUMBER_OF_IMAGES] = {
	RESOURCE_ID_IMAGE_TIME_MIDNIGHT_BLACK,
	RESOURCE_ID_IMAGE_TIME_EARLY_MORNING_BLACK,
	RESOURCE_ID_IMAGE_TIME_MORNING_BLACK,
	RESOURCE_ID_IMAGE_TIME_DAYTIME_BLACK,
	RESOURCE_ID_IMAGE_TIME_LUNCHTIME_BLACK,
	RESOURCE_ID_IMAGE_TIME_AFTERNOON_BLACK,
	RESOURCE_ID_IMAGE_TIME_EVENING_BLACK,
	RESOURCE_ID_IMAGE_TIME_NIGHT_BLACK,
	RESOURCE_ID_IMAGE_TIME_BLANK_BLACK,
};

void SetCurrentWordImage(P4FWORD* P4FWord)
{
	//Debug
	//P4FWord->iCurrentWord = 3;
	
	P4FWord->imgWord_WHITE = gbitmap_create_with_resource(WORD_IMAGES_WHITE[P4FWord->iCurrentWord]);
	P4FWord->imgWord_BLACK = gbitmap_create_with_resource(WORD_IMAGES_BLACK[P4FWord->iCurrentWord]);	
}

void SetWordImageWHITE(P4FWORD* P4FWord, Layer* layer)
{
	////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Entered SetWordImage_WHITE");
	P4FWord->State.bExistsImgWHITE = true;
	P4FWord->bmplyrWord_WHITE = bitmap_layer_create(P4FWord->fraWordFrame);
	P4FWord->State.bExistsLyrWHITE = true;
	bitmap_layer_set_bitmap(P4FWord->bmplyrWord_WHITE, P4FWord->imgWord_WHITE); //or .layer?
	P4FWord->State.bSetImgWHITE = true;
	bitmap_layer_set_background_color(P4FWord->bmplyrWord_WHITE,GColorClear);
	bitmap_layer_set_compositing_mode(P4FWord->bmplyrWord_WHITE, GCompOpOr);
	layer_add_child(layer, bitmap_layer_get_layer(P4FWord->bmplyrWord_WHITE));
	P4FWord->State.bSetLyrWHITE = true;
	bitmap_layer_set_alignment(P4FWord->bmplyrWord_WHITE, GAlignLeft);
	layer_mark_dirty(bitmap_layer_get_layer(P4FWord->bmplyrWord_WHITE));
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Left SetWordImage_WHITE");
}

void SetWordImageBLACK(P4FWORD* P4FWord, Layer* layer)
{
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Entered SetWordImage_BLACK");
	P4FWord->State.bExistsImgBLACK = true;
	P4FWord->bmplyrWord_BLACK = bitmap_layer_create(P4FWord->fraWordFrame);
	P4FWord->State.bExistsLyrBLACK = true;
	bitmap_layer_set_bitmap(P4FWord->bmplyrWord_BLACK, P4FWord->imgWord_BLACK); //or .layer?
	P4FWord->State.bSetImgBLACK = true;
	bitmap_layer_set_background_color(P4FWord->bmplyrWord_BLACK,GColorClear);
	bitmap_layer_set_compositing_mode(P4FWord->bmplyrWord_BLACK, GCompOpClear);
	layer_add_child(layer, bitmap_layer_get_layer(P4FWord->bmplyrWord_BLACK));
	P4FWord->State.bSetLyrBLACK = true;
	bitmap_layer_set_alignment(P4FWord->bmplyrWord_BLACK, GAlignLeft);
	layer_mark_dirty(bitmap_layer_get_layer(P4FWord->bmplyrWord_BLACK));
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Left SetWordImage_BLACK");
}
			
void SetWordImages(P4FWORD* P4FWord, Layer* layer)
{	
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Entered SetWord Images");
	SetWordImageWHITE(P4FWord, layer);
	SetWordImageBLACK(P4FWord, layer);
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Left SetWord Images");
}

void GetWordInteger(P4FWORD* P4FWord, int iCurrentHour)
{
    switch (iCurrentHour)
    {
        case 0:
            P4FWord->iCurrentWord = 0; //midnight
            break;
        case 3:
        case 4:
        case 5:
            P4FWord->iCurrentWord = 1; //early morning
            break;
        case 6:
        case 7:
        case 8:
            P4FWord->iCurrentWord = 2; //morning
            break;
        case 9:
        case 10:
        case 11:
			P4FWord->iCurrentWord = 3; //daytime
            break;
		case 12:
            P4FWord->iCurrentWord = 4;//lunchtime
            break;
        case 13:
        case 14:
        case 15:
        case 16:
		case 17:
            P4FWord->iCurrentWord = 5;//afternoon
            break;
        case 18:
        case 19:
		case 20:
            P4FWord->iCurrentWord = 6;//evening
            break;
        case 21:
        case 22:
        case 23:
        case 24:
		case 1:
		case 2:
            P4FWord->iCurrentWord = 7;//night
            break;
    }    
}

void RemoveAndDeIntWord(P4FWORD* P4FWord)
{
	if (P4FWord->State.bInitialized)
	{
		if (P4FWord->State.bExistsLyrWHITE && P4FWord->State.bSetLyrWHITE)
		{
			layer_remove_from_parent(bitmap_layer_get_layer(P4FWord->bmplyrWord_WHITE));
			bitmap_layer_destroy(P4FWord->bmplyrWord_WHITE);
			P4FWord->State.bSetLyrWHITE = false;
			P4FWord->State.bExistsLyrWHITE = false;
		}
			
		if (P4FWord->State.bExistsLyrBLACK && P4FWord->State.bSetLyrBLACK)
		{
			layer_remove_from_parent(bitmap_layer_get_layer(P4FWord->bmplyrWord_BLACK));
			bitmap_layer_destroy(P4FWord->bmplyrWord_BLACK);
			P4FWord->State.bSetLyrBLACK = false;
			P4FWord->State.bExistsLyrBLACK = false;
		}
			
		if (P4FWord->State.bExistsImgWHITE && P4FWord->State.bSetImgWHITE)
		{
			gbitmap_destroy(P4FWord->imgWord_WHITE);	
			P4FWord->State.bSetImgWHITE = false;
			P4FWord->State.bExistsImgWHITE = false;
		}
			
		if (P4FWord->State.bExistsImgBLACK && P4FWord->State.bSetImgBLACK)
		{
			gbitmap_destroy(P4FWord->imgWord_BLACK);	
			P4FWord->State.bSetImgBLACK = false;
			P4FWord->State.bExistsImgBLACK = false;
		}	
					
		if (P4FWord->State.bAniSlideLeftStart_BLACKExists)
		{		
			property_animation_destroy(P4FWord->aniSlideLeftStart_BLACK);
			P4FWord->State.bAniSlideLeftStart_BLACKExists = false;
		}

		if (P4FWord->State.bAniSlideLeftEnd_BLACKExists)
		{
			property_animation_destroy(P4FWord->aniSlideLeftEnd_BLACK);
			P4FWord->State.bAniSlideLeftEnd_BLACKExists = false;
		}
		
		if (P4FWord->State.bAniSlideLeftStart_WHITEExists)
		{
			property_animation_destroy(P4FWord->aniSlideLeftStart_WHITE);
			P4FWord->State.bAniSlideLeftStart_WHITEExists = false;
		}
				
		if (P4FWord->State.bAniSlideLeftEnd_WHITEExists)
		{
			property_animation_destroy(P4FWord->aniSlideLeftEnd_WHITE);
			P4FWord->State.bAniSlideLeftEnd_WHITEExists = false;
		}
	}
}


bool ChangeWordCheck(P4FWORD* P4FWord)
{
	if (P4FWord->iPreviousWord != P4FWord->iCurrentWord)
		return true;
	else
		return false;
}

void GetSetCurrentWord(P4FWORD* P4FWord,Layer* layer, bool bIsWTWT)
{
	if (P4FWord->State.bInitialized && ChangeWordCheck(P4FWord))
	{
		RemoveAndDeIntWord(P4FWord);
		if (bIsWTWT)
		{
			//SetWTWTFrameOffset(P4FWord, P4FWord->iCurrentWord);
			P4FWord->fraWordFrame = P4FWord->fraWordHold;
		}
		else
		{
			P4FWord->fraWordFrame = P4FWord->fraWordStart;
		}
		SetCurrentWordImage(P4FWord);
		SetWordImages(P4FWord, layer);
		P4FWord->iPreviousWord = P4FWord->iCurrentWord;
	}
}

void P4FWORD_INIT(P4FWORD* P4FWord)
{
	P4FWord->fraWordStart = WATCH_FRA_WORD;
	P4FWord->State.bInitialized = true;
	P4FWord->iPreviousWord = 99;
	P4FWord->iCurrentWord = 88;
}

void P4FWORD_DEINIT(P4FWORD* P4FWord)
{
	P4FWord->State.bInitialized = false;	
}
