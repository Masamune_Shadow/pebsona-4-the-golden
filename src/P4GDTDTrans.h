#include "P4GWatchface.h" //because we need what it has in its includes

typedef struct
{
	bool bInitialized;
	bool bDTDTransitioning;
	bool bFontsInitialized;
	bool bExists;
	bool bDTDTInvertExists;

	bool bAniOpeningExists;
	bool bAniClosingLeftExists;	
	bool bAniClosingRightExists;
	bool bAniHoldExists;	
	bool bAniPreviousMonthExists;
	bool bAniPreviousYearExists;
	bool bAniCurrentMonthExists;
	bool bAniCurrentYearExists;
		
	bool bChangeDay;
	bool bPreviousMonthExists;
	bool bPreviousYearExists;
	bool bCurrentMonthExists;
	bool bCurrentYearExists;
	bool bInvertExists;
	bool bClosingLeftExists;
	bool bClosingRightExists;
	bool bPastPointofNoReturn;
	bool bMonthChanged;
	bool bYearChanged;
	bool bClosing;
		//all of the following end
	//static bool bLocated = false;
} DTDTStateVars;
	
typedef struct
{
	bool bDayExists;
} DTDTRANSDAYStateVars;

typedef struct
{
	DTDTRANSDAYStateVars State;
		
	P4FDATE 	P4FDate;
	//P4FWEATHER	P4FWeather;
	P4FSPLITWEATHER	P4FSplitWeather;
	
	//Animations have mooved into their minor classes.
}DTDTRANSDAY;


//QT:DT
typedef struct
{
	//P4GWATCHFACE 	P4GWatchface;
	//Layer*			lyrP4GWatchface;
	Layer* 			lyrP4GDTDTrans;
	DTDTRANSDAY Day[NUMBER_OF_DAYS]; //4
	DTDTStateVars State;
	
	InverterLayer* invlyrInvertDTDT;
	InverterLayer* 	invlyrInvert;
	
	
	//BitmapLayer* 	bmplyrDayTrans;
	//BitmapLayer* 	bmplyrBackground;
	BitmapLayer* 	bmplyrClosingLeft;
	BitmapLayer* 	bmplyrClosingRight;
	
	TextLayer* 		txtlyrPreviousMonth;
	TextLayer* 		txtlyrPreviousYear;
	TextLayer* 		txtlyrCurrentMonth;
	TextLayer* 		txtlyrCurrentYear;
	
	GRect fraBackground;
	GRect fraPreviousMonth;
	GRect fraPreviousYear;
	GRect fraCurrentMonth;
	GRect fraCurrentYear;
	GRect fraCoverLeft;
	GRect fraCoverRight;
	GRect fraCoverPreviousMonth;
	GRect fraCoverPreviousYear;
	
	GRect fraStart;
	GRect fraEnd;
	GRect fraClosingLeftStart;
	GRect fraClosingLeftEnd;
	GRect fraClosingRightStart;
	GRect fraClosingRightEnd;
		
	//Window* window;
	PropertyAnimation* aniOpening;
	PropertyAnimation* aniClosingLeft;	
	PropertyAnimation* aniClosingRight;
	PropertyAnimation* aniHold;	
	PropertyAnimation* aniPreviousMonth;
	PropertyAnimation* aniPreviousYear;
	PropertyAnimation* aniCurrentMonth;
	PropertyAnimation* aniCurrentYear;
	P4FFONTS	P4FDTDTRANSFonts;
}P4GDTDTRANS;

//extern Window* window;

//void DTDTRANS_INIT(P4FFONTS inputP4FFonts, Window* window);
void DTDTRANS_INIT(bool bInputChangeDay, Window* window);
void P4GDTDTRANS_DEINIT();
//void DTDTRANS_INIT( Layer* inputLyrP4GWatchface, Window* window);
void StartDTDTrans();
void DTOpeningAnimationStopped(Animation* animation, bool finished, void* context);
void DTSetupSlidingFrames();
void DTSlidingAnimationStopped(Animation* animation, bool finished, void* context);
void DTHoldAnimationStopped(Animation* animation, bool finished, void* context);
void DTClosingAnimationStopped(Animation* animation, bool finished, void* context);
void RemoveAndDeIntDTDTRANSDAY();
void RemoveAndDeIntAllDTDT();
