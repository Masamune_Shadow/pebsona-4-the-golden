#include "P4FWeather.h"
//bool fPersist = false;

static uint32_t WEATHER_IMAGES[] = 
{
	RESOURCE_ID_IMAGE_WEATHER_SUN,
	RESOURCE_ID_IMAGE_WEATHER_CLOUD,
	RESOURCE_ID_IMAGE_WEATHER_RAIN,
	RESOURCE_ID_IMAGE_WEATHER_SNOW,
	RESOURCE_ID_IMAGE_WEATHER_UNKNOWN,
	RESOURCE_ID_IMAGE_WEATHER_LIGHTNING,
};

static uint32_t WEATHER_IMAGES_SQUARE[] = 
{
	RESOURCE_ID_IMAGE_SQUARE_SUN,
	RESOURCE_ID_IMAGE_SQUARE_CLOUD,
	RESOURCE_ID_IMAGE_SQUARE_RAIN,
	RESOURCE_ID_IMAGE_SQUARE_SNOW,
	RESOURCE_ID_IMAGE_SQUARE_UNKNOWN,
	RESOURCE_ID_IMAGE_SQUARE_LIGHTNING,
};

static uint32_t WEATHER_IMAGES_SPLIT_TOP[] = 
{
	RESOURCE_ID_IMAGE_SQUARE_SUN_TOP,
	RESOURCE_ID_IMAGE_SQUARE_CLOUD_TOP,
	RESOURCE_ID_IMAGE_SQUARE_RAIN_TOP,
	RESOURCE_ID_IMAGE_SQUARE_SNOW_TOP,
	RESOURCE_ID_IMAGE_SQUARE_UNKNOWN_TOP,
	RESOURCE_ID_IMAGE_SQUARE_LIGHTNING_TOP,
};

//we should only EVER need reg. top and  inverted bottom with comp_op clear
static uint32_t WEATHER_IMAGES_SPLIT_BOTTOM_INVERT[] = 
{
	RESOURCE_ID_IMAGE_SQUARE_SUN_BOTTOM_INVERT,
	RESOURCE_ID_IMAGE_SQUARE_CLOUD_BOTTOM_INVERT,
	RESOURCE_ID_IMAGE_SQUARE_RAIN_BOTTOM_INVERT,
	RESOURCE_ID_IMAGE_SQUARE_SNOW_BOTTOM_INVERT,
	RESOURCE_ID_IMAGE_SQUARE_UNKNOWN_BOTTOM_INVERT,
	RESOURCE_ID_IMAGE_SQUARE_LIGHTNING_BOTTOM_INVERT,
};

void SetWeatherImageTOP(P4FWEATHER* P4FWeatherSplit_TOP, WEATHERIMAGE WeatherImage, Layer* layer)
{
	
	if (P4FWeatherSplit_TOP->bUseSquare)
	{
		if (P4FWeatherSplit_TOP->State.bSplit)
		{
			P4FWeatherSplit_TOP->imgWeather = gbitmap_create_with_resource(WEATHER_IMAGES_SPLIT_TOP[WeatherImage]);
		}
		else
		{	
			P4FWeatherSplit_TOP->imgWeather = gbitmap_create_with_resource(WEATHER_IMAGES_SQUARE[WeatherImage]);
		}
	}
	else
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "doesn't square");
		P4FWeatherSplit_TOP->imgWeather = gbitmap_create_with_resource(WEATHER_IMAGES[WeatherImage]);
	}
	
	P4FWeatherSplit_TOP->bmplyrWeather = bitmap_layer_create(P4FWeatherSplit_TOP->fraWeatherFrame);
	bitmap_layer_set_bitmap(P4FWeatherSplit_TOP->bmplyrWeather, P4FWeatherSplit_TOP->imgWeather); //or .layer?
	P4FWeatherSplit_TOP->State.bSetImg = true;
	bitmap_layer_set_background_color(P4FWeatherSplit_TOP->bmplyrWeather,GColorClear);
	bitmap_layer_set_compositing_mode(P4FWeatherSplit_TOP->bmplyrWeather, GCompOpOr);
	if (P4FWeatherSplit_TOP->bUseSquare)
	{
		layer_insert_below_sibling(bitmap_layer_get_layer(P4FWeatherSplit_TOP->bmplyrWeather), layer);
	}
	else
	{
		layer_add_child(layer, bitmap_layer_get_layer(P4FWeatherSplit_TOP->bmplyrWeather));
	}
	P4FWeatherSplit_TOP->State.bSetLyr = true;
	layer_mark_dirty(bitmap_layer_get_layer(P4FWeatherSplit_TOP->bmplyrWeather));
	P4FWeatherSplit_TOP->CurrentWeather = WeatherImage;
}

void SetWeatherImageBOTTOM(P4FWEATHER* P4FWeatherSplit_BOTTOM, WEATHERIMAGE WeatherImage, Layer* layer)
{
	//P4FWeatherSplit_BOTTOM->State.bSplit = false;
	//special because i always need the layers created even if there isn't a bottom.
	if (P4FWeatherSplit_BOTTOM->State.bSplit)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Bottom Weather Set");
		P4FWeatherSplit_BOTTOM->bmplyrWeather = bitmap_layer_create(P4FWeatherSplit_BOTTOM->fraWeatherFrame);
		P4FWeatherSplit_BOTTOM->imgWeather = gbitmap_create_with_resource(WEATHER_IMAGES_SPLIT_BOTTOM_INVERT[WeatherImage]);
		bitmap_layer_set_bitmap(P4FWeatherSplit_BOTTOM->bmplyrWeather, P4FWeatherSplit_BOTTOM->imgWeather);
		P4FWeatherSplit_BOTTOM->State.bSetImg = true;
		bitmap_layer_set_compositing_mode(P4FWeatherSplit_BOTTOM->bmplyrWeather, GCompOpClear);	
		bitmap_layer_set_background_color(P4FWeatherSplit_BOTTOM->bmplyrWeather,GColorClear);
		layer_insert_below_sibling(bitmap_layer_get_layer(P4FWeatherSplit_BOTTOM->bmplyrWeather), layer);
		P4FWeatherSplit_BOTTOM->State.bSetLyr = true;
		layer_mark_dirty(bitmap_layer_get_layer(P4FWeatherSplit_BOTTOM->bmplyrWeather));
		P4FWeatherSplit_BOTTOM->CurrentWeather = WeatherImage;
	}
}

void SetSplitWeatherImages(P4FSPLITWEATHER* P4FSplitWeather, WEATHERIMAGE WeatherImage_TOP,  WEATHERIMAGE WeatherImage_BOTTOM, Layer* layer)
{		
	SetWeatherImageTOP(&P4FSplitWeather->P4FSplitWeather_TOP, WeatherImage_TOP, layer);
	SetWeatherImageBOTTOM(&P4FSplitWeather->P4FSplitWeather_BOTTOM, WeatherImage_BOTTOM, layer);
}

void RemoveAndDeIntWeather(P4FWEATHER* P4FWeather)
{
	if (P4FWeather->State.bSetLyr)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete Weather Layer");
		layer_remove_from_parent(bitmap_layer_get_layer(P4FWeather->bmplyrWeather));
		bitmap_layer_destroy(P4FWeather->bmplyrWeather);	
		P4FWeather->State.bSetLyr = false;
	}
				
	if (P4FWeather->State.bSetImg)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete Weather Image");
		gbitmap_destroy(P4FWeather->imgWeather);	
		P4FWeather->State.bSetImg = false;
	}	
	
	if (P4FWeather->State.bAniSlideLeftImgExists && !animation_is_scheduled(&P4FWeather->aniSlideLeftImg->animation))
	{
		//will be triggered on the de init of the program, after the mass unschedule.
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete Weather Animation");
		property_animation_destroy(P4FWeather->aniSlideLeftImg);
		P4FWeather->State.bAniSlideLeftImgExists = false;
	}
}

void GetSetWeatherImage(P4FWEATHER* P4FWeather,WEATHERIMAGE WeatherImage, Layer* layer)
{
	if (P4FWeather->State.bInitialized)
	{
		RemoveAndDeIntWeather(P4FWeather);
	}
	SetWeatherImageTOP(P4FWeather,WeatherImage, layer);
}

void GetSetSplitWeatherImages(P4FSPLITWEATHER* P4FSplitWeather,WEATHERIMAGE WeatherImage_TOP, WEATHERIMAGE WeatherImage_BOTTOM, Layer* layer)
{
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Get Set Split");
	if (P4FSplitWeather->State.bInitialized)
		RemoveAndDeIntSplitWeather(P4FSplitWeather);

	if ((WeatherImage_TOP == WEATHER_UNKNOWN || WeatherImage_BOTTOM == WEATHER_UNKNOWN) &&
		!(WeatherImage_TOP == WEATHER_UNKNOWN && WeatherImage_BOTTOM == WEATHER_UNKNOWN))
	{
		//setup this way so that if the top is unknown, it'll take the bottom
		//which will result in them == the same, meaning that the logic for "top only" will be hit
		if (WeatherImage_TOP == WEATHER_UNKNOWN)
		{
			WeatherImage_TOP = WeatherImage_BOTTOM;
		}
		else if (WeatherImage_BOTTOM == WEATHER_UNKNOWN)
		{
			WeatherImage_BOTTOM = WeatherImage_TOP;
		}
		//&& (WeatherImage_TOP != WEATHER_UNKNOWN && WeatherImage_BOTTOM != WEATHER_UNKNOWN)
	}
		
	if (WeatherImage_TOP == WeatherImage_BOTTOM)
	{
		P4FSplitWeather->State.bSplit = false;
		P4FSplitWeather->P4FSplitWeather_TOP.State.bSplit = false;
		P4FSplitWeather->P4FSplitWeather_BOTTOM.State.bSplit = false;
		
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Create Weather TOP Only");
		//SetSplitWeatherImages(&P4FSplitWeather->P4FSplitWeather_TOP, WeatherImage_TOP, layer);
		//SetWeatherImageTOP(&P4FSplitWeather->P4FSplitWeather_TOP,WeatherImage_TOP, layer);
		//RemoveAndDeIntWeather(&P4FSplitWeather->P4FSplitWeather_BOTTOM);
		//P4FWEATHER_DEINIT(&P4FSplitWeather->P4FSplitWeather_BOTTOM);
	}
	/*else
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Create Weather TOP AND BOTTOM ");
		SetSplitWeatherImages(P4FSplitWeather,WeatherImage_TOP,WeatherImage_BOTTOM, layer);
	}*/
	SetSplitWeatherImages(P4FSplitWeather,WeatherImage_TOP,WeatherImage_BOTTOM, layer);
}

void RemoveAndDeIntSplitWeather(P4FSPLITWEATHER* P4FSplitWeather)
{
	RemoveAndDeIntWeather(&P4FSplitWeather->P4FSplitWeather_TOP);
	RemoveAndDeIntWeather(&P4FSplitWeather->P4FSplitWeather_BOTTOM);
}

WEATHERIMAGE Int2WEATHERIMAGE(int input)
{
	if (input ==  0)
		return WEATHER_SUN;
	else if (input ==  1)
		return WEATHER_CLOUD;
	else if (input ==  2)
		return WEATHER_RAIN;
	else if (input ==  3)
		return WEATHER_SNOW;
	else if (input == 5)
		return WEATHER_LIGHTNING;
	else
		return WEATHER_UNKNOWN;
}

void P4FSPLITWEATHER_DTDT_INIT(P4FSPLITWEATHER* P4FSplitWeather, int iDay, bool bInputChangeDay, Layer* layer)
{	
	//int iImage = 4;
	
	switch(iDay)
	{
		case 0: //YES
			if (bInputChangeDay)
			{
				P4FSplitWeather->P4FSplitWeather_TOP.fraWeatherFrame = DT_FRA_DAY_1_IMAGE;
				P4FSplitWeather->P4FSplitWeather_TOP.fraImgEnd = DT_FRA_DAY_0_IMAGE;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.fraWeatherFrame = DT_FRA_DAY_1_IMAGE;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.fraImgEnd = DT_FRA_DAY_0_IMAGE;
			}
			else
			{
				P4FSplitWeather->P4FSplitWeather_TOP.fraWeatherFrame = DT_FRA_DAY_0_IMAGE;
				P4FSplitWeather->P4FSplitWeather_TOP.fraImgEnd = DT_FRA_DAY_0_IMAGE;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.fraWeatherFrame = DT_FRA_DAY_0_IMAGE;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.fraImgEnd = DT_FRA_DAY_0_IMAGE;
			}
			
			if (fPersist)
			{
				P4FSplitWeather->P4FSplitWeather_TOP.CurrentWeather = Int2WEATHERIMAGE(GetPersistKey(P4FPKEY_YES_MOR_ICON));
				////P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather = WEATHER_UNKNOWN;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather = Int2WEATHERIMAGE(GetPersistKey(P4FPKEY_YES_EVE_ICON));
			}
			else
			{
				//P4FSplitWeather->P4FSplitWeather_TOP.CurrentWeather = WEATHER_UNKNOWN;
				//P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather = WEATHER_UNKNOWN;
				P4FSplitWeather->P4FSplitWeather_TOP.CurrentWeather = WEATHER_LIGHTNING;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather = WEATHER_LIGHTNING;
			}
			break;
		case 1: //TOD
			if (bInputChangeDay)
			{
				P4FSplitWeather->P4FSplitWeather_TOP.fraWeatherFrame = DT_FRA_DAY_2_IMAGE;
				P4FSplitWeather->P4FSplitWeather_TOP.fraImgEnd = DT_FRA_DAY_1_IMAGE;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.fraWeatherFrame = DT_FRA_DAY_2_IMAGE;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.fraImgEnd = DT_FRA_DAY_1_IMAGE;
			}
			else
			{
				P4FSplitWeather->P4FSplitWeather_TOP.fraWeatherFrame = DT_FRA_DAY_1_IMAGE;
				P4FSplitWeather->P4FSplitWeather_TOP.fraImgEnd = DT_FRA_DAY_1_IMAGE;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.fraWeatherFrame = DT_FRA_DAY_1_IMAGE;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.fraImgEnd = DT_FRA_DAY_1_IMAGE;
			}
			
			if (fPersist)
			{
				P4FSplitWeather->P4FSplitWeather_TOP.CurrentWeather = Int2WEATHERIMAGE(GetPersistKey(P4FPKEY_TOD_MOR_ICON));
				////P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather = WEATHER_UNKNOWN;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather = Int2WEATHERIMAGE(GetPersistKey(P4FPKEY_TOD_EVE_ICON));
			}
			else
			{
				//P4FSplitWeather->P4FSplitWeather_TOP.CurrentWeather = WEATHER_UNKNOWN;
				//P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather = WEATHER_UNKNOWN;
				P4FSplitWeather->P4FSplitWeather_TOP.CurrentWeather = WEATHER_SUN;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather = WEATHER_SUN;
			}
			break;
		case 2: //TOM
		
			if (bInputChangeDay)
			{
				P4FSplitWeather->P4FSplitWeather_TOP.fraWeatherFrame = DT_FRA_DAY_3_IMAGE;
				P4FSplitWeather->P4FSplitWeather_TOP.fraImgEnd = DT_FRA_DAY_2_IMAGE;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.fraWeatherFrame = DT_FRA_DAY_3_IMAGE;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.fraImgEnd = DT_FRA_DAY_2_IMAGE;
			}
			else
			{
				P4FSplitWeather->P4FSplitWeather_TOP.fraWeatherFrame = DT_FRA_DAY_2_IMAGE;
				P4FSplitWeather->P4FSplitWeather_TOP.fraImgEnd = DT_FRA_DAY_2_IMAGE;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.fraWeatherFrame = DT_FRA_DAY_2_IMAGE;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.fraImgEnd = DT_FRA_DAY_2_IMAGE;
			}
			
			
			if (fPersist)
			{
				P4FSplitWeather->P4FSplitWeather_TOP.CurrentWeather = Int2WEATHERIMAGE(GetPersistKey(P4FPKEY_TOM_MOR_ICON));
				////P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather = WEATHER_UNKNOWN;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather = Int2WEATHERIMAGE(GetPersistKey(P4FPKEY_TOM_EVE_ICON));
			}
			else
			{
				//P4FSplitWeather->P4FSplitWeather_TOP.CurrentWeather = WEATHER_UNKNOWN;
				//P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather = WEATHER_UNKNOWN;
				P4FSplitWeather->P4FSplitWeather_TOP.CurrentWeather = WEATHER_LIGHTNING;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather = WEATHER_RAIN;
			}
			break;
		case 3: //DAT
			
			if (bInputChangeDay)
			{
				P4FSplitWeather->P4FSplitWeather_TOP.fraWeatherFrame = DT_FRA_DAY_4_IMAGE;
				P4FSplitWeather->P4FSplitWeather_TOP.fraImgEnd = DT_FRA_DAY_3_IMAGE;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.fraWeatherFrame = DT_FRA_DAY_4_IMAGE;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.fraImgEnd = DT_FRA_DAY_3_IMAGE;
			}
			else
			{
				P4FSplitWeather->P4FSplitWeather_TOP.fraWeatherFrame = DT_FRA_DAY_3_IMAGE;
				P4FSplitWeather->P4FSplitWeather_TOP.fraImgEnd = DT_FRA_DAY_3_IMAGE;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.fraWeatherFrame = DT_FRA_DAY_3_IMAGE;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.fraImgEnd = DT_FRA_DAY_3_IMAGE;
			}
			
			
			if (fPersist)
			{
				P4FSplitWeather->P4FSplitWeather_TOP.CurrentWeather = Int2WEATHERIMAGE(GetPersistKey(P4FPKEY_DAT_MOR_ICON));
				P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather = Int2WEATHERIMAGE(GetPersistKey(P4FPKEY_DAT_EVE_ICON));
				
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Top DTDT 3 weather is %d", P4FSplitWeather->P4FSplitWeather_TOP.CurrentWeather);
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Bottom DTDT 3 weather is %d", P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather);
				////P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather = WEATHER_UNKNOWN;
			}				
			else
			{
				//P4FSplitWeather->P4FSplitWeather_TOP.CurrentWeather = WEATHER_UNKNOWN;
				//P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather = WEATHER_UNKNOWN;
				P4FSplitWeather->P4FSplitWeather_TOP.CurrentWeather = WEATHER_SUN;
				P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather = WEATHER_LIGHTNING;
			}
			break;		
	}
	P4FSplitWeather->State.bSplit = true;
	P4FSplitWeather->P4FSplitWeather_TOP.State.bSplit = true;
	P4FSplitWeather->P4FSplitWeather_BOTTOM.State.bSplit = true;
	P4FSplitWeather->P4FSplitWeather_TOP.bUseSquare = true;
	P4FSplitWeather->P4FSplitWeather_BOTTOM.bUseSquare = true;
	P4FSplitWeather->State.bInitialized = true;
	if (iDay == 0)
	{
		P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather = WEATHER_UNKNOWN;
		P4FSplitWeather->P4FSplitWeather_TOP.CurrentWeather = WEATHER_UNKNOWN;
		GetSetSplitWeatherImages(P4FSplitWeather,P4FSplitWeather->P4FSplitWeather_TOP.CurrentWeather,P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather, layer);
	}
	else
	{
		GetSetSplitWeatherImages(P4FSplitWeather,P4FSplitWeather->P4FSplitWeather_TOP.CurrentWeather,P4FSplitWeather->P4FSplitWeather_BOTTOM.CurrentWeather, layer);
	}
}

void P4FWEATHER_INIT(P4FWEATHER* P4FWeather, Layer* layer)
{	
	if (!P4FWeather->State.bInitialized)
	{
		P4FWeather->fraWeatherFrame = WATCH_FRA_WEATHER;
		P4FWeather->State.bInitialized = true;
		P4FWeather->State.bSplit = false;
		if (fPersist)
		{
			P4FWeather->CurrentWeather = Int2WEATHERIMAGE(GetPersistKey(P4FPKEY_CUR_ICON));
		}
		else
		{
			P4FWeather->CurrentWeather = WEATHER_SNOW;
		}
		GetSetWeatherImage(P4FWeather, P4FWeather->CurrentWeather, layer);
	}
}

void P4FWEATHER_DEINIT(P4FWEATHER* P4FWeather)
{
	P4FWeather->State.bInitialized = false;	
}

void P4FSPLITWEATHER_DEINIT(P4FSPLITWEATHER* P4FSplitWeather)
{
	P4FWEATHER_DEINIT(&P4FSplitWeather->P4FSplitWeather_TOP);
	P4FWEATHER_DEINIT(&P4FSplitWeather->P4FSplitWeather_BOTTOM);
	P4FSplitWeather->State.bInitialized = false;	
}
