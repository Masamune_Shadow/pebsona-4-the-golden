#include "P4GWatchface.h"

extern P4GWATCHFACE P4GWatchface;


void CreateImgBackground()
{
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "use TV colors is %i", bUseTVColors);
	/*
	if (bUseTVColors)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "use TV colors");
		P4GWatchface.imgBackground = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_BACKGROUND_INVERT);
	}
	else
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "dont use TV colors");
		P4GWatchface.imgBackground = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_BACKGROUND);
	}
	*/
	P4GWatchface.imgBackground = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_BACKGROUND);
	bitmap_layer_set_bitmap(P4GWatchface.lyrP4GWatchface, P4GWatchface.imgBackground);
	P4GWatchface.State.bSetBackground = true;
	/*
	if (bUseInvertedColors && !P4GWatchface.State.bInvertExists)
	{
		P4GWatchface.invlyrInvertWatchface = inverter_layer_create(P4GWatchface.fraBackground);
		layer_insert_above_sibling(inverter_layer_get_layer(P4GWatchface.invlyrInvertWatchface), bitmap_layer_get_layer(P4GWatchface.lyrP4GWatchface));
		P4GWatchface.State.bInvertExists = true;
	}
	*/
}

void CreateBackground()
{
	if (!P4GWatchface.State.bSetBackground)
	{
		P4GWatchface.lyrP4GWatchface = bitmap_layer_create(P4GWatchface.fraBackground);
		CreateImgBackground();
	}
}

void ChangeBackground(Window* window)
{
	if (P4GWatchface.State.bSetBackground)
	{
		
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "TV Colors change %d", bUseTVColors);
		RemoveAndDeInt("CHANGEBG");
		CreateImgBackground();

		layer_insert_above_sibling(bitmap_layer_get_layer(P4GWatchface.lyrP4GWatchface),window_get_root_layer(window));
		
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Use Inverted colors %d invert exists %d", bUseInvertedColors, P4GWatchface.State.bInvertExists);
		/*
		if (bUseInvertedColors && !P4GWatchface.State.bInvertExists)
		{
			P4GWatchface.invlyrInvertWatchface = inverter_layer_create(P4GWatchface.fraBackground);
			layer_insert_above_sibling(inverter_layer_get_layer(P4GWatchface.invlyrInvertWatchface), bitmap_layer_get_layer(P4GWatchface.lyrP4GWatchface));
			P4GWatchface.State.bInvertExists = true;
		}
		*/
		
	}
}
/*!!TEMPERATURE*/
void ChangeTemperature()
{
	if (P4GWatchface.P4FTemperature.State.bInitialized)
	{
		layer_set_hidden(text_layer_get_layer(P4GWatchface.P4FTemperature.txtlyrP4FTemperature), !bUseTemperature); //coming back from a DTDT or WTWT
		layer_mark_dirty(text_layer_get_layer(P4GWatchface.P4FTemperature.txtlyrP4FTemperature));
	}
}
	
void SetupWatchface(struct tm* t, Window* window)
{
	if (!P4GWatchface.State.bExists && !P4GWatchface.State.bInitialized)
	{
		P4GWatchface.State.bInitializing = true;
		P4GWatchface.State.bExists = true;
		P4GWatchface.fraBackground = WATCH_FRA_BACKGROUND;
		CreateBackground();
		InitDateFonts(&P4GWatchface.P4FFonts);
		P4FTIME_INIT(&P4GWatchface.P4FTime, bitmap_layer_get_layer(P4GWatchface.lyrP4GWatchface));
		P4FWORD_INIT(&P4GWatchface.P4FWord);
		P4FDATE_INIT(&P4GWatchface.P4FDate, bitmap_layer_get_layer(P4GWatchface.lyrP4GWatchface));	
		P4FWEATHER_INIT(&P4GWatchface.P4FWeather, bitmap_layer_get_layer(P4GWatchface.lyrP4GWatchface));	
		P4FTEMPERATURE_INIT(&P4GWatchface.P4FTemperature, bitmap_layer_get_layer(P4GWatchface.lyrP4GWatchface));
		
		text_layer_set_font(P4GWatchface.P4FDate.txtlyrDate, P4GWatchface.P4FFonts.P4FDateFonts.fontDate);
		text_layer_set_font(P4GWatchface.P4FDate.txtlyrDateWord, P4GWatchface.P4FFonts.P4FDateFonts.fontAbbr);
		text_layer_set_font(P4GWatchface.P4FTime.txtlyrP4FTime, P4GWatchface.P4FFonts.P4FDateFonts.fontTime);
		//text_layer_set_font(P4GWatchface.P4FTemperature.txtlyrP4FTemperature, P4GWatchface.P4FFonts.P4FDateFonts.fontAbbr);
		text_layer_set_font(P4GWatchface.P4FTemperature.txtlyrP4FTemperature, P4GWatchface.P4FFonts.P4FDateFonts.fontTime);
		//layer_set_hidden(text_layer_get_layer(P4GWatchface.P4FTemperature.txtlyrP4FTemperature), !bUseTemperature); //coming back from a DTDT or WTWT
		
		
		P4GWatchface.State.bInitializing = false;
		P4GWatchface.State.bInitialized = true;
		ChangeTemperature();
		//P4GWatchface.P4FWord.iPreviousWord = P4GWatchface.P4FWord.iCurrentWord;
		P4GWatchface.P4FTime.iPreviousHour = P4GWatchface.P4FTime.iCurrentHour;
					
		layer_add_child(window_get_root_layer(window), bitmap_layer_get_layer(P4GWatchface.lyrP4GWatchface));
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Use Inverted colors %d invert exists %d", bUseInvertedColors, P4GWatchface.State.bInvertExists);
		/*
		if (bUseInvertedColors && P4GWatchface.State.bInvertExists)
		{
			P4GWatchface.invlyrInvertWatchface = inverter_layer_create(P4GWatchface.fraBackground);
			layer_add_child(window_get_root_layer(window),inverter_layer_get_layer(P4GWatchface.invlyrInvertWatchface));		
		}
		*/
		
	}	
	else
	{
	//Second chance stuff
		/*
		if (!P4GWatchface.P4FFonts.P4FDateFonts.State.bInitialized)
		{
			InitDateFonts(&P4GWatchface.P4FFonts);	
		}	
		*/
		if (!P4GWatchface.P4FTime.State.bInitialized)
		{
			//P4FTIME_INIT(&P4GWatchface.P4FTime);
			P4FTIME_INIT(&P4GWatchface.P4FTime, bitmap_layer_get_layer(P4GWatchface.lyrP4GWatchface));
		}
		
		if (!P4GWatchface.P4FWord.State.bInitialized)
		{
			P4FWORD_INIT(&P4GWatchface.P4FWord);	
		}
		
		if (!P4GWatchface.P4FDate.State.bInitialized)
		{
			P4FDATE_INIT(&P4GWatchface.P4FDate, bitmap_layer_get_layer(P4GWatchface.lyrP4GWatchface));	
		}
		
		if (!P4GWatchface.P4FWeather.State.bInitialized)
		{
			P4FWEATHER_INIT(&P4GWatchface.P4FWeather, bitmap_layer_get_layer(P4GWatchface.lyrP4GWatchface));		
		}

		if (!P4GWatchface.P4FTemperature.State.bInitialized)
		{
			P4FTEMPERATURE_INIT(&P4GWatchface.P4FTemperature, bitmap_layer_get_layer(P4GWatchface.lyrP4GWatchface));
		}
		
	}	
	
	//Set the stuff
	if (P4GWatchface.P4FTime.State.bInitialized)
	{
		GetSetTime(&P4GWatchface.P4FTime);
		text_layer_set_font(P4GWatchface.P4FTime.txtlyrP4FTime, P4GWatchface.P4FFonts.P4FDateFonts.fontTime);
	}
	
	if (P4GWatchface.P4FWord.State.bInitialized)
	{
		GetWordInteger(&P4GWatchface.P4FWord,t->tm_hour);
		GetSetCurrentWord(&P4GWatchface.P4FWord, bitmap_layer_get_layer(P4GWatchface.lyrP4GWatchface), false);
	}

	if (P4GWatchface.P4FDate.State.bInitialized)
	{
		SetCurrentDateAndDateWord(&P4GWatchface.P4FDate,t);
	}
	
	if (P4GWatchface.P4FWeather.State.bInitialized)
	{
		GetSetWeatherImage(&P4GWatchface.P4FWeather,P4GWatchface.P4FWeather.CurrentWeather, bitmap_layer_get_layer(P4GWatchface.lyrP4GWatchface));
	}
	
	if (P4GWatchface.P4FTemperature.State.bInitialized)
	{
		ChangeTemperature();
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Set Current Temp from Watchface");
		SetCurrentTemperature(&P4GWatchface.P4FTemperature);
		//text_layer_set_font(P4GWatchface.P4FTemperature.txtlyrP4FTemperature, P4GWatchface.P4FFonts.P4FDateFonts.fontAbbr);
		text_layer_set_font(P4GWatchface.P4FTemperature.txtlyrP4FTemperature, P4GWatchface.P4FFonts.P4FDateFonts.fontTime);
	}
	
	
	if (P4GWatchface.P4FFonts.P4FDateFonts.State.bInitialized && P4GWatchface.P4FDate.State.bInitialized)
	{		
		text_layer_set_font(P4GWatchface.P4FDate.txtlyrDate, P4GWatchface.P4FFonts.P4FDateFonts.fontDate);
		text_layer_set_font(P4GWatchface.P4FDate.txtlyrDateWord, P4GWatchface.P4FFonts.P4FDateFonts.fontAbbr);
	}
	
	P4GWatchface.P4FDate.iCurrentDay =  t->tm_mday;
	P4GWatchface.P4FDate.iPreviousDay =  t->tm_mday;
	layer_set_hidden(bitmap_layer_get_layer(P4GWatchface.lyrP4GWatchface), false); //coming back from a DTDT or WTWT
	/*
	if (bUseInvertedColors && P4GWatchface.State.bInvertExists)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "hide invert");
		layer_set_hidden(inverter_layer_get_layer(P4GWatchface.invlyrInvertWatchface), false); //coming back from a DTDT or WTWT
	}
	*/
	
}

void RemoveAndDeInt(char* cToRnD)
{
	bool bAll = false;
	bool bTransition = false;
	
	if (strcmp(cToRnD,ALL) == 0)
	 	bAll = true;
	
	if (strcmp(cToRnD,TRANSITION) == 0)
		bTransition = true;
		
	if (strcmp(cToRnD,DATEFONT) == 0 || bAll)
		RemoveAndDeIntDateFonts(&P4GWatchface.P4FFonts.P4FDateFonts);
	if (strcmp(cToRnD,WORD) == 0 || (bAll || bTransition))
		RemoveAndDeIntWord(&P4GWatchface.P4FWord);
	if (strcmp(cToRnD,DATE) == 0 || (bAll || bTransition))
		RemoveAndDeIntAllDate(&P4GWatchface.P4FDate);
	if (strcmp(cToRnD,WEATHER) == 0 || (bAll || bTransition))
		RemoveAndDeIntWeather(&P4GWatchface.P4FWeather);
	//!!TEMPERATURE
	if (strcmp(cToRnD,TEMPERATURE) == 0 || (bAll || bTransition))
		P4FTEMPERATURE_DEINIT(&P4GWatchface.P4FTemperature);
		
		//RemoveAndDeIntTemperature(&P4GWatchface.P4FTemperature);
	/*if (strcmp(cToRnD,TIME) == 0 || (bAll || bTransition))
		DeIntTime(&P4GWatchface.P4FTime);		
	*/

	if (strcmp(cToRnD,WATCH) == 0 || bAll)
	{
		if (P4GWatchface.State.bSetBackground) 
		{
			/*
			if (P4GWatchface.State.bInvertExists)
			{
				layer_remove_from_parent(inverter_layer_get_layer(P4GWatchface.invlyrInvertWatchface));
				inverter_layer_destroy(P4GWatchface.invlyrInvertWatchface);
				P4GWatchface.State.bInvertExists = false;
			}
			*/
			layer_remove_from_parent(bitmap_layer_get_layer(P4GWatchface.lyrP4GWatchface));
			bitmap_layer_destroy(P4GWatchface.lyrP4GWatchface);
			gbitmap_destroy(P4GWatchface.imgBackground);
			P4GWatchface.State.bSetBackground = false;			
		}
	}
	
	if (strcmp(cToRnD,CHANGEBG) == 0 || bAll)
	{
		if (P4GWatchface.State.bSetBackground) 
		{
			/*
			if (P4GWatchface.State.bInvertExists)
			{
				layer_remove_from_parent(inverter_layer_get_layer(P4GWatchface.invlyrInvertWatchface));
				inverter_layer_destroy(P4GWatchface.invlyrInvertWatchface);
				P4GWatchface.State.bInvertExists = false;
			}
			*/
			gbitmap_destroy(P4GWatchface.imgBackground);
			P4GWatchface.State.bSetBackground = false;			
		}
	}
}

void P4GWATCHFACE_DEINIT(char* cToRnD)
{
	bool bAll = false;
	bool bTransition = false;
	
	if (strcmp(cToRnD,ALL) == 0)
	 	bAll = true;
	 	
	 if (strcmp(cToRnD,TRANSITION) == 0)
		bTransition = true;

	if (strcmp(cToRnD,DATEFONT) == 0 || bAll)
		P4FDATEFONTS_DEINIT(&P4GWatchface.P4FFonts);
	if (strcmp(cToRnD,TIME) == 0 || (bAll || bTransition))
		P4FTIME_DEINIT(&P4GWatchface.P4FTime);
	if (strcmp(cToRnD,WORD) == 0 || (bAll || bTransition))
		P4FWORD_DEINIT(&P4GWatchface.P4FWord);
	if (strcmp(cToRnD,DATE) == 0 || (bAll || bTransition))
		P4FDATE_DEINIT(&P4GWatchface.P4FDate);
	if (strcmp(cToRnD,WEATHER) == 0 || (bAll || bTransition))
		P4FWEATHER_DEINIT(&P4GWatchface.P4FWeather);
	/*!!TEMPERATURE*/
	 if (strcmp(cToRnD,TEMPERATURE) == 0 || (bAll || bTransition))
		P4FTEMPERATURE_DEINIT(&P4GWatchface.P4FTemperature);
		
		
	if (bAll)
	{
		P4GWatchface.State.bInitialized = false;
		//P4GWatchface.State.bInvertExists = false;
		P4GWatchface.State.bExists = false;
	}
}
