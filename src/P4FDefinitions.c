#include "P4FDefinitions.h"

void SetPersistV(uint32_t key, int iInput);
void SetPersistDT(uint32_t keyDT, long int liInputTime);
void SetPersistVDTAndFindDT(uint32_t key, int iInput);
void SetPersistVDT(uint32_t key, int iInput, uint32_t keyDT, long int liInputTime);

void CheckPersistDays()
{

	if (fPersist)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "In PersistDays");

		uint32_t key = (uint32_t)P4FPKEY_JUNK; //default today mor
		uint32_t keyDT = (uint32_t)P4FPKEY_JUNK;
		uint32_t iKeyStart = (uint32_t)P4FPKEY_JUNK;
		uint32_t iNewKeyOffset = (uint32_t)P4FPKEY_JUNK;
		uint32_t iDeleteStart = (uint32_t)P4FPKEY_JUNK;
		uint32_t NewKey = (uint32_t)P4FPKEY_JUNK;
		uint32_t NewKeyDT = (uint32_t)P4FPKEY_JUNK;
		int iNewValue = 0;
		long int iNewDT = (long int)0;
		
		long int liPTime = (long int)0;
				
		bool bFound = false;
		bool bKeyChange = false;
		//bool bAllInvalid = false;
		bool bResetToDefault = false;
		
		
		
		//Set the midnight time correctly
		time_t tMidnightTime = time(NULL);
		struct tm* tmMidnightTime = localtime(&tMidnightTime);
		long int liMidnightTime = (long int)tMidnightTime;
		liMidnightTime-=(long int)(tmMidnightTime->tm_hour*60*60);//one hour
		liMidnightTime-=(long int)(tmMidnightTime->tm_min*60);
		liMidnightTime-=(long int)(tmMidnightTime->tm_sec*1);
		
		if (persist_exists((uint32_t)P4FPKEY_TOD_MOR_ICON) && persist_exists((uint32_t)P4FPKEY_LASTMOD_TOD_MOR_DT))
		{
			//iKeyValue = persist_read_int(key);
			liPTime = (long int)persist_read_int(P4FPKEY_LASTMOD_TOD_MOR_DT);	
			bFound = true;
		}
		
		if (bFound)
		{
			if (liMidnightTime != liPTime)
			{
				if (liMidnightTime < liPTime)
				{
					//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Deleteing All");
					//They went back in time, default to all ?'s
					iDeleteStart = (uint32_t)1;
					bResetToDefault = true;
				}
				else if (liMidnightTime > liPTime)
				{
					long int liUseTime;
					liUseTime = liMidnightTime - liPTime;	//(since it's confirmed >)
					
					//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "UseTime Remainder = %ld", liUseTime);
					if (liUseTime <= (long int)86400) //+1 day
					{
						//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Moving left 1 day");
						bKeyChange = true;
						iDeleteStart = (uint32_t)7;
						iKeyStart = (uint32_t)3;
						iNewKeyOffset= (uint32_t)2;
					}
					else if (liUseTime > (long int)86400 && liUseTime <= (long int)172800) //+2
					{
						bKeyChange = true;
						iDeleteStart = (uint32_t)5;
						iKeyStart = (uint32_t)5;
						iNewKeyOffset= (uint32_t)4;
						//5 & 6 become new 1 & 2
						//7 & 8 become new 3 & 4
						//delete 5-8
					}
					else if (liUseTime > (long int)172800 && liUseTime <= (long int)259200)
					{
						bKeyChange = true;
						iDeleteStart = (uint32_t)3;
						iKeyStart = (uint32_t)7;
						iNewKeyOffset= (uint32_t)6;
						//7 & 8 become new 1 & 2
						//delete 3-8
					}
					else if (liUseTime > (long int)259200)
					{
						iDeleteStart = (uint32_t)1;
						bResetToDefault = true;
					}
				}
				
				if (bKeyChange)
				{
					//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Keychange = true");
					for (uint32_t i = iKeyStart; i < (uint32_t)9; i++)
					{
						//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Gone through for loop %d times", (int)i);
						key = i;
						keyDT = (key+(uint32_t)10);
						
						if (persist_exists(key) && persist_exists(keyDT))
						{
												
							NewKey = (i-iNewKeyOffset);
							iNewValue = persist_read_int(key);
							NewKeyDT = (key+(uint32_t)10);
							iNewDT = (long int)persist_read_int(keyDT);	
						
							SetPersistVDT(NewKey,iNewValue, NewKeyDT, iNewDT);
						}
					}	
				}
				
				if (bKeyChange || bResetToDefault)
				{
					//if (bResetToDefault)
						//SetPersistV(P4FPKEY_CUR_ICON,4);
					//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Deleting keys starting from Key %lu", iDeleteStart);
					for (uint32_t i = iDeleteStart; i < (uint32_t)9; i++)
					{
						key = i;
						keyDT = key + (uint32_t)10;
						//if (persist_exists(key))
						//{
							SetPersistV(key,4);
							//persist_delete(key);  //delete the old key stuff, it's out of date.
						//}
						if (persist_exists(keyDT))
						{
							persist_delete(keyDT);
						}
					}
				}
			}
			//Set the current value to to the tod value (if applicable)
			time_t tCurrentTime = time(NULL);
			struct tm* tmCurrentTime = localtime(&tCurrentTime);
			if (tmCurrentTime->tm_hour <= 12)
			{
				iNewValue = persist_read_int(P4FPKEY_TOD_MOR_ICON);
				SetPersistV(P4FPKEY_CUR_ICON,iNewValue);
			}
			else
			{
				iNewValue = persist_read_int(P4FPKEY_TOD_EVE_ICON);
				SetPersistV(P4FPKEY_CUR_ICON,iNewValue);
			}
		}
	}
}

void CheckPersistBools()
{
	if (fPersist)
	{
		/*
		if (persist_exists((uint32_t)P4FPKEY_USE_TV_COLORS))
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "reading from persist");
			bUseTVColors = persist_read_bool(P4FPKEY_USE_TV_COLORS);
		}
		else
			bUseTVColors = false;
			*/
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Persist Key %lu read is  %d ", (uint32_t)P4FPKEY_USE_TV_COLORS,bUseTVColors);

		if (persist_exists((uint32_t)P4FPKEY_USE_INVERTED_AXIS))
			bUseInvertedAxis = persist_read_bool(P4FPKEY_USE_INVERTED_AXIS);
		else
			bUseInvertedAxis = false;
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Persist Key %lu read is  %d ", (uint32_t)P4FPKEY_USE_INVERTED_AXIS,bUseInvertedAxis);	

		if (persist_exists((uint32_t)P4FPKEY_USE_TEMPERATURE))
			bUseTemperature = persist_read_bool(P4FPKEY_USE_TEMPERATURE);
		else
			bUseTemperature = false;
		
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Persist Key %lu read is  %d ", (uint32_t)P4FPKEY_USE_TEMPERATURE,bUseTemperature);
	/*
	if (persist_exists((uint32_t)P4FPKEY_USE_INVERTED_COLORS))
			bUseInvertedColors = persist_read_bool(P4FPKEY_USE_INVERTED_COLORS);
		else    
			bUseInvertedColors = false;
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Persist Key %lu read is  %d ", (uint32_t)P4FPKEY_USE_TEMPERATURE,bUseTemperature);
	*/
	}
	
}


void SetPersistV(uint32_t key, int iInput)
{
	if (fPersist)
	{
		bool bValid = true;
		if (persist_exists(key))
		{
			int iTemp = persist_read_int(key);
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Persist Key %lu read is  %d ", key,iTemp);
			if (iTemp == iInput)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Persist Key %lu value of %d was not saved", key, iInput);
				bValid = false;
			}
		}
		
		if (bValid)
		{
			persist_write_int(key,iInput);
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Persist Key %lu value of %d was saved", key, iInput);
		}
	}
}

void SetPersistDT(uint32_t keyDT, long int liInputTime)
{	
	if (fPersist)
	{
		bool bValid = true;
		if (persist_exists(keyDT))
		{
			long int liPersistTime = (long int)persist_read_int(keyDT);
			if (liInputTime == liPersistTime)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Persist KeyDT %lu value was not saved", keyDT);
				bValid = false;			
			}
		}
		
		if (bValid)
		{
			persist_write_int(keyDT, liInputTime);
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Persist KeyDT %lu value of %ld was saved", keyDT, liInputTime);
		}
	}
}

void SetPersistVDT(uint32_t key, int iInput, uint32_t keyDT, long int liInputTime)
{
	SetPersistV(key, iInput);
	SetPersistDT(keyDT, liInputTime);
}

void SetPersistVDTAndFindDT(uint32_t key, int iInput)
{
	if (fPersist)
	{
		//bool fCheckMorning = false;
		uint32_t keyDT = P4FPKEY_JUNK;
		time_t tUseTime = time(NULL);
		
		//SetToMidnight(&tUseTime);
		struct tm* tmUseTime = localtime(&tUseTime);
		long int liUseTime = (long int) tUseTime;
		//SetToMidnight, or 00:00:00 
		liUseTime-=(long int)(tmUseTime->tm_hour*60*60);//one hour
		liUseTime-=(long int)(tmUseTime->tm_min*60);
		liUseTime-=(long int)(tmUseTime->tm_sec*1);
		
		switch (key)
		{
			/*
			case P4FPKEY_YES_MOR_ICON:
				keyDT	 = P4FPKEY_LASTMOD_YES_MOR_DT;
				liUseTime-=(24*60*60); //-1 day
			break;
			case P4FPKEY_YES_EVE_ICON:
				keyDT	 = P4FPKEY_LASTMOD_YES_EVE_DT;
				liUseTime-=(12*60*60); //- 1 day (but at noon)
			break;
			*/
			case P4FPKEY_TOD_MOR_ICON:
				keyDT	 = P4FPKEY_LASTMOD_TOD_MOR_DT;
				//liUseTime; //don't need to modify
			break;
			case P4FPKEY_TOD_EVE_ICON:
				keyDT	 = P4FPKEY_LASTMOD_TOD_EVE_DT;
				liUseTime+=(12*60*60);
			break;
			case P4FPKEY_TOM_MOR_ICON:
				keyDT	 = P4FPKEY_LASTMOD_TOM_MOR_DT;
				liUseTime+=(24*60*60); //+1 day
			break;
			case P4FPKEY_TOM_EVE_ICON:
				keyDT	 = P4FPKEY_LASTMOD_TOM_EVE_DT;
				liUseTime+=(36*60*60); //+1 day (but at noon)
			break;
			case P4FPKEY_DAT_MOR_ICON:
				keyDT	 = P4FPKEY_LASTMOD_DAT_MOR_DT;
				liUseTime+=(2*24*60*60); //+1 day
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Persist Key %lu has time of %ld", keyDT, liUseTime);
			break;
			case P4FPKEY_DAT_EVE_ICON:
				keyDT	 = P4FPKEY_LASTMOD_DAT_EVE_DT;
				liUseTime+=(60*60*60); //+1 day
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Persist Key %lu has time of %ld", keyDT, liUseTime);
			break;	
			default:
				keyDT	 = P4FPKEY_JUNK;
			break;
		}
		SetPersistVDT(key, iInput, keyDT, liUseTime);
	}
}

void SetPersistBool(uint32_t keyInput, bool bInputBool)
{	
	if (fPersist)
	{
		bool bValid = true;
		if (persist_exists(keyInput))
		{
			bool bPersistBool = persist_read_bool(keyInput);
			if (bPersistBool == bInputBool)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Persist KeyDT %lu value was not saved", keyInput);
				bValid = false;			
			}
		}
		
		if (bValid)
		{
			persist_write_bool(keyInput, bInputBool);
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Persist KeyDT %lu value of %d was saved", keyInput, bInputBool);
		}
	}
}


int GetPersistKey(uint32_t key)
{
	int iReturn = 4;
	if (fPersist)
	{
		bool bFound = false;
		
		if (persist_exists(key))// && persist_exists(keyDT))
		{
			iReturn = persist_read_int(key);
			bFound = true;
		}
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Getting Key %lu value of %d ", key, iReturn);
		if (bFound)
		{
			return iReturn;
		}
		else
		{
			iReturn = 4;
		}
	}
	return iReturn;
}
