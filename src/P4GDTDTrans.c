#include "P4GDTDTrans.h"

extern P4GDTDTRANS P4GDTDTrans;
//extern P4GWATCHFACE	P4GWatchface;

//QT:DTCLOSE QT:DTTOP

void DTClosingAnimationStopped(Animation* animation, bool finished, void* context)
{
	RemoveAndDeIntAllDTDT();	
}

//QT:DTHOLDSTOP
void DTHoldAnimationStopped(Animation* animation, bool finished, void* context) 
{
	if (finished)
	{
		/*if (P4GDTDTrans.State.bAniHoldExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Hold");
			property_animation_destroy(P4GDTDTrans.aniHold);
			P4GDTDTrans.State.bAniHoldExists = false;
		}*/
		//the stuff is off.
		//implement state variables for all of the following
	
		P4GDTDTrans.State.bPastPointofNoReturn = true;
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "hold stopped get");
		P4GDTDTrans.fraClosingLeftStart 	= DT_FRA_CLOSING_LEFT_START;
		P4GDTDTrans.fraClosingLeftEnd 		= DT_FRA_CLOSING_LEFT_END;
		P4GDTDTrans.fraClosingRightStart 	= DT_FRA_CLOSING_RIGHT_START;	
		P4GDTDTrans.fraClosingRightEnd 		= DT_FRA_CLOSING_RIGHT_END;
		
		layer_set_frame(bitmap_layer_get_layer(P4GDTDTrans.bmplyrClosingLeft),P4GDTDTrans.fraClosingLeftStart);
		layer_mark_dirty(bitmap_layer_get_layer(P4GDTDTrans.bmplyrClosingLeft));
		layer_set_frame(bitmap_layer_get_layer(P4GDTDTrans.bmplyrClosingRight),P4GDTDTrans.fraClosingRightStart);
		layer_mark_dirty(bitmap_layer_get_layer(P4GDTDTrans.bmplyrClosingRight));
		
		//setup closing animation
		P4GDTDTrans.aniClosingLeft = property_animation_create_layer_frame(bitmap_layer_get_layer(P4GDTDTrans.bmplyrClosingLeft),&P4GDTDTrans.fraClosingLeftStart,&P4GDTDTrans.fraClosingLeftEnd);
		P4GDTDTrans.State.bAniClosingLeftExists = true;	
		animation_set_duration( &P4GDTDTrans.aniClosingLeft->animation, DT_ANI_CLOSING_DURATION);
		animation_set_delay(&P4GDTDTrans.aniClosingLeft->animation, DT_ANI_CLOSING_DELAY);
		
		P4GDTDTrans.aniClosingRight = property_animation_create_layer_frame(bitmap_layer_get_layer(P4GDTDTrans.bmplyrClosingRight),&P4GDTDTrans.fraClosingRightStart,&P4GDTDTrans.fraClosingRightEnd);
		P4GDTDTrans.State.bAniClosingRightExists = true;
		animation_set_duration( &P4GDTDTrans.aniClosingRight->animation, DT_ANI_CLOSING_DURATION);
		animation_set_delay(&P4GDTDTrans.aniClosingRight->animation, DT_ANI_CLOSING_DELAY);
		
		//handlers on the right only.
		animation_set_handlers(&P4GDTDTrans.aniClosingRight->animation, (AnimationHandlers){
		.stopped = (AnimationStoppedHandler)DTClosingAnimationStopped
		}, NULL);
		animation_schedule(&P4GDTDTrans.aniClosingLeft->animation);
		animation_schedule(&P4GDTDTrans.aniClosingRight->animation);
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "hold stopped left");
		RemoveAndDeIntDTDTRANSDAY();
	}
}

void DTSlidingAnimationStopped(Animation* animation, bool finished, void* context) 
{
	if (finished)
	{
		if (P4GDTDTrans.State.bMonthChanged)
		{	
			if (P4GDTDTrans.State.bPreviousMonthExists)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT P Month");
				layer_remove_from_parent(text_layer_get_layer(P4GDTDTrans.txtlyrPreviousMonth));
				text_layer_destroy(P4GDTDTrans.txtlyrPreviousMonth);		
				P4GDTDTrans.State.bPreviousMonthExists = false;
			}
			if (P4GDTDTrans.State.bAniPreviousMonthExists)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Previous M");
				property_animation_destroy(P4GDTDTrans.aniPreviousMonth);
				P4GDTDTrans.State.bAniPreviousMonthExists = false;
			}
		}
		else
		{
			if (P4GDTDTrans.State.bCurrentMonthExists)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Cur");
				layer_remove_from_parent(text_layer_get_layer(P4GDTDTrans.txtlyrCurrentMonth));
				text_layer_destroy(P4GDTDTrans.txtlyrCurrentMonth);		
				P4GDTDTrans.State.bCurrentMonthExists = false;
			}
			if (P4GDTDTrans.State.bAniCurrentMonthExists)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Month");
				property_animation_destroy(P4GDTDTrans.aniCurrentMonth);
				P4GDTDTrans.State.bAniCurrentMonthExists = false;
			}
		}
		
		if (P4GDTDTrans.State.bYearChanged)
		{
			if (P4GDTDTrans.State.bPreviousYearExists)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Prev Y");
				layer_remove_from_parent(text_layer_get_layer(P4GDTDTrans.txtlyrPreviousYear));
				text_layer_destroy(P4GDTDTrans.txtlyrPreviousYear);		
				P4GDTDTrans.State.bPreviousYearExists = false;
			}
			if (P4GDTDTrans.State.bAniPreviousYearExists)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Prev Year");
				property_animation_destroy(P4GDTDTrans.aniPreviousYear);
				P4GDTDTrans.State.bAniPreviousYearExists = false;
			}
		}
		else
		{	
			if (P4GDTDTrans.State.bCurrentYearExists)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT C Y");
				layer_remove_from_parent(text_layer_get_layer(P4GDTDTrans.txtlyrCurrentYear));
				text_layer_destroy(P4GDTDTrans.txtlyrCurrentYear);	
				P4GDTDTrans.State.bCurrentYearExists = false;
			}
			if (P4GDTDTrans.State.bAniCurrentYearExists)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Year");
				property_animation_destroy(P4GDTDTrans.aniCurrentYear);
				P4GDTDTrans.State.bAniCurrentYearExists = false;
			}
		}
		
		P4GDTDTrans.aniHold = property_animation_create_layer_frame(P4GDTDTrans.lyrP4GDTDTrans,&P4GDTDTrans.fraBackground,&P4GDTDTrans.fraBackground);
		P4GDTDTrans.State.bAniHoldExists = true;
		animation_set_duration( &P4GDTDTrans.aniHold->animation, DT_ANI_HOLD_DURATION); //500
		animation_set_handlers(&P4GDTDTrans.aniHold->animation, (AnimationHandlers){
		.stopped = (AnimationStoppedHandler)DTHoldAnimationStopped
		}, NULL);
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "slide stopped left");
		animation_schedule(&P4GDTDTrans.aniHold->animation);
	}
}


//QT:DTOPENSTOP
//QT:DTSLIDING
void DTSetupSlidingFrames()
{
	
	int iOffset;

	for (int i = 0; i < 4; i++)
	{
		
		if (i == 0)
			iOffset = -1;
		else if (i == 1)
			iOffset = 0;
		else if (i == 2)
			iOffset = 1;
		else if (i == 3)
			iOffset = 2;
		
			P4FDATE_DTDT_INIT(&P4GDTDTrans.Day[i].P4FDate,i, P4GDTDTrans.State.bChangeDay, P4GDTDTrans.lyrP4GDTDTrans);//inverter_layer_get_layer(P4GDTDTrans.invlyrInvert));
			
			text_layer_set_font(P4GDTDTrans.Day[i].P4FDate.txtlyrDate, P4GDTDTrans.P4FDTDTRANSFonts.P4FDateFonts.fontDate);
			text_layer_set_font(P4GDTDTrans.Day[i].P4FDate.txtlyrDateWord, P4GDTDTrans.P4FDTDTRANSFonts.P4FDateFonts.fontAbbr);
			
			P4GDTDTrans.Day[i].State.bDayExists = true;
			SetOffsetWordDayOfWeek(&P4GDTDTrans.Day[i].P4FDate, iOffset);
			
			//Sadly, I've had to do this so that Yesterday is only one (Due to Pebble reducing the amount of memory that can be actively used both onscreen at once AND in the background before crashing).  But this should make it so that works
			/*
			 * if (i == 0)
			{
				P4FSPLITWEATHER_DTDT_INIT(&P4GDTDTrans.Day[i].P4FSplitWeather,i,P4GDTDTrans.State.bChangeDay, inverter_layer_get_layer(P4GDTDTrans.invlyrInvert));	
				GetSetSplitWeatherImages(&P4GDTDTrans.Day[i].P4FSplitWeather,P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.CurrentWeather,P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.CurrentWeather, inverter_layer_get_layer(P4GDTDTrans.invlyrInvert));
			}
			else
			{
				*/
				P4FSPLITWEATHER_DTDT_INIT(&P4GDTDTrans.Day[i].P4FSplitWeather,i,P4GDTDTrans.State.bChangeDay, inverter_layer_get_layer(P4GDTDTrans.invlyrInvert));	
				GetSetSplitWeatherImages(&P4GDTDTrans.Day[i].P4FSplitWeather,P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.CurrentWeather,P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.CurrentWeather, inverter_layer_get_layer(P4GDTDTrans.invlyrInvert));
			//}

	}
}


//QT:DTOPENSTOP
void DTOpeningAnimationStopped(Animation* animation, bool finished, void* context) 
{	
	if (finished)
	{
		
		/*if (P4GDTDTrans.State.bAniOpeningExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Opening");
			property_animation_destroy(P4GDTDTrans.aniOpening);
			P4GDTDTrans.State.bAniOpeningExists = false;
		}*/
		
		time_t tTime = time(NULL);
		struct tm* tm = localtime(&tTime);
		//previous month
		
		if (P4GDTDTrans.State.bChangeDay)
		{
			P4GDTDTrans.txtlyrPreviousMonth = text_layer_create(P4GDTDTrans.fraPreviousMonth);
		}
		else
		{
			P4GDTDTrans.txtlyrPreviousMonth = text_layer_create(P4GDTDTrans.fraCurrentMonth);
		}
		P4GDTDTrans.State.bPreviousMonthExists = true;
		text_layer_set_text_color(P4GDTDTrans.txtlyrPreviousMonth, GColorWhite);
		text_layer_set_background_color(P4GDTDTrans.txtlyrPreviousMonth, GColorClear);
		text_layer_set_font(P4GDTDTrans.txtlyrPreviousMonth, P4GDTDTrans.P4FDTDTRANSFonts.P4FDTDTFonts.fontMonth);
		text_layer_set_text_alignment(P4GDTDTrans.txtlyrPreviousMonth, GTextAlignmentCenter);
		//layer_add_child(P4GDTDTrans.lyrP4GDTDTrans, text_layer_get_layer(P4GDTDTrans.txtlyrPreviousMonth));
		layer_insert_below_sibling(text_layer_get_layer(P4GDTDTrans.txtlyrPreviousMonth),inverter_layer_get_layer(P4GDTDTrans.invlyrInvert));
		
		GetSetPreviousDayMonthYear(tm);

		//Get the day of month #
		static char txtPreviousMonth[] = "00";
		strftime(txtPreviousMonth, sizeof(txtPreviousMonth), "%m", tm);
		
		if (txtPreviousMonth[0] == '1')
			text_layer_set_text(P4GDTDTrans.txtlyrPreviousMonth, (txtPreviousMonth));	
		else
			text_layer_set_text(P4GDTDTrans.txtlyrPreviousMonth, &txtPreviousMonth[1]);
	
		if (P4GDTDTrans.State.bChangeDay)
		{
			P4GDTDTrans.txtlyrPreviousYear = text_layer_create(P4GDTDTrans.fraPreviousYear);
		}
		else
		{
			P4GDTDTrans.txtlyrPreviousYear = text_layer_create(P4GDTDTrans.fraCurrentYear);
		}
		P4GDTDTrans.State.bPreviousYearExists = true;
		text_layer_set_text_color(P4GDTDTrans.txtlyrPreviousYear, GColorWhite);
		text_layer_set_background_color(P4GDTDTrans.txtlyrPreviousYear, GColorClear);
		text_layer_set_font(P4GDTDTrans.txtlyrPreviousYear, P4GDTDTrans.P4FDTDTRANSFonts.P4FDTDTFonts.fontYear);
		text_layer_set_text_alignment(P4GDTDTrans.txtlyrPreviousYear, GTextAlignmentCenter);
		//layer_add_child(P4GDTDTrans.lyrP4GDTDTrans, text_layer_get_layer(P4GDTDTrans.txtlyrPreviousYear));
		layer_insert_below_sibling(text_layer_get_layer(P4GDTDTrans.txtlyrPreviousYear),inverter_layer_get_layer(P4GDTDTrans.invlyrInvert));
		
		static char txtPreviousYear[] = "0000";
		strftime(txtPreviousYear, sizeof(txtPreviousYear), "%Y", tm);
		text_layer_set_text(P4GDTDTrans.txtlyrPreviousYear, txtPreviousYear);
		
		
		tm = localtime(&tTime);
		//current month
		if (P4GDTDTrans.State.bChangeDay)
		{
			P4GDTDTrans.txtlyrCurrentMonth = text_layer_create(P4GDTDTrans.fraCurrentMonth);
		}
		else
		{
			P4GDTDTrans.txtlyrCurrentMonth = text_layer_create(P4GDTDTrans.fraPreviousMonth);
		}
		P4GDTDTrans.State.bCurrentMonthExists = true;
		text_layer_set_text_color(P4GDTDTrans.txtlyrCurrentMonth, GColorWhite);
		text_layer_set_background_color(P4GDTDTrans.txtlyrCurrentMonth, GColorClear);
		text_layer_set_font(P4GDTDTrans.txtlyrCurrentMonth, P4GDTDTrans.P4FDTDTRANSFonts.P4FDTDTFonts.fontMonth);
		text_layer_set_text_alignment(P4GDTDTrans.txtlyrCurrentMonth, GTextAlignmentCenter);
		//layer_add_child(P4GDTDTrans.lyrP4GDTDTrans, text_layer_get_layer(P4GDTDTrans.txtlyrCurrentMonth));
		layer_insert_below_sibling(text_layer_get_layer(P4GDTDTrans.txtlyrCurrentMonth),inverter_layer_get_layer(P4GDTDTrans.invlyrInvert));
	
		static char txtCurrentMonth[] = "00";
		strftime(txtCurrentMonth, sizeof(txtCurrentMonth), "%m", tm);
		if (txtCurrentMonth[0] == '1')
			text_layer_set_text(P4GDTDTrans.txtlyrCurrentMonth, (txtCurrentMonth));	
		else
			text_layer_set_text(P4GDTDTrans.txtlyrCurrentMonth, &txtCurrentMonth[1]);

		if (P4GDTDTrans.State.bChangeDay)
		{
			P4GDTDTrans.txtlyrCurrentYear = text_layer_create(P4GDTDTrans.fraCurrentYear);
		}
		else
		{
			P4GDTDTrans.txtlyrCurrentYear = text_layer_create(P4GDTDTrans.fraPreviousYear);
		}
		P4GDTDTrans.State.bCurrentYearExists = true;
		text_layer_set_text_color(P4GDTDTrans.txtlyrCurrentYear, GColorWhite);
		text_layer_set_background_color(P4GDTDTrans.txtlyrCurrentYear, GColorClear);
		text_layer_set_font(P4GDTDTrans.txtlyrCurrentYear, P4GDTDTrans.P4FDTDTRANSFonts.P4FDTDTFonts.fontYear);
		text_layer_set_text_alignment(P4GDTDTrans.txtlyrCurrentYear, GTextAlignmentCenter);
		//layer_add_child(P4GDTDTrans.lyrP4GDTDTrans, text_layer_get_layer(P4GDTDTrans.txtlyrCurrentYear));
		layer_insert_below_sibling(text_layer_get_layer(P4GDTDTrans.txtlyrCurrentYear),inverter_layer_get_layer(P4GDTDTrans.invlyrInvert));
		
		static char txtCurrentYear[] = "0000";
		strftime(txtCurrentYear, sizeof(txtCurrentYear), "%Y", tm);
		text_layer_set_text(P4GDTDTrans.txtlyrCurrentYear, txtCurrentYear);
		
		
		
		P4GDTDTrans.fraCoverLeft = DT_FRA_COVER_LEFT;
		P4GDTDTrans.fraCoverRight = DT_FRA_COVER_RIGHT;
		
		
		//create cover left
		P4GDTDTrans.bmplyrClosingLeft = bitmap_layer_create(P4GDTDTrans.fraCoverLeft);
		P4GDTDTrans.State.bClosingLeftExists = true;
		bitmap_layer_set_background_color(P4GDTDTrans.bmplyrClosingLeft, GColorBlack);
		layer_add_child(P4GDTDTrans.lyrP4GDTDTrans, bitmap_layer_get_layer(P4GDTDTrans.bmplyrClosingLeft));
		
		//create cover right
		P4GDTDTrans.bmplyrClosingRight = bitmap_layer_create(P4GDTDTrans.fraCoverRight);
		P4GDTDTrans.State.bClosingRightExists = true;
		bitmap_layer_set_background_color(P4GDTDTrans.bmplyrClosingRight, GColorBlack);
		layer_add_child(P4GDTDTrans.lyrP4GDTDTrans, bitmap_layer_get_layer(P4GDTDTrans.bmplyrClosingRight));
		
		DTSetupSlidingFrames();
		
		/*
		P4GDTDTrans.aniPreviousMonth = property_animation_create_layer_frame(text_layer_get_layer(P4GDTDTrans.txtlyrPreviousMonth),&P4GDTDTrans.fraPreviousMonth,&P4GDTDTrans.fraPreviousMonth);
		P4GDTDTrans.State.bAniPreviousMonthExists = true;
		animation_set_duration(&P4GDTDTrans.aniPreviousMonth, DT_ANI_SLIDE_DURATION);
		animation_set_delay(&P4GDTDTrans.aniPreviousMonth, DT_ANI_SLIDE_DELAY);
		animation_set_handlers(&P4GDTDTrans.aniPreviousMonth, (AnimationHandlers){
		.stopped = (AnimationStoppedHandler)DTSlidingAnimationStopped
		}, NULL);
		
		animation_schedule(&P4GDTDTrans.aniPreviousMonth);	
		*/

		for (int i = 0; i < 4; i++)
		{
			
			P4GDTDTrans.Day[i].P4FDate.aniSlideLeftWord = property_animation_create_layer_frame(text_layer_get_layer(P4GDTDTrans.Day[i].P4FDate.txtlyrDateWord),&P4GDTDTrans.Day[i].P4FDate.fraWordFrame,&P4GDTDTrans.Day[i].P4FDate.fraWordEnd);
			P4GDTDTrans.Day[i].P4FDate.State.bAniSlideLeftWordExists = true;
			
			P4GDTDTrans.Day[i].P4FDate.aniSlideLeftDay = property_animation_create_layer_frame(text_layer_get_layer(P4GDTDTrans.Day[i].P4FDate.txtlyrDate),&P4GDTDTrans.Day[i].P4FDate.fraDateFrame, &P4GDTDTrans.Day[i].P4FDate.fraDateEnd);
			P4GDTDTrans.Day[i].P4FDate.State.bAniSlideLeftDayExists = true;
			
			animation_set_duration(&P4GDTDTrans.Day[i].P4FDate.aniSlideLeftWord->animation, DT_ANI_SLIDE_DURATION);
			animation_set_delay(&P4GDTDTrans.Day[i].P4FDate.aniSlideLeftWord->animation, DT_ANI_SLIDE_DELAY);
			animation_schedule(&P4GDTDTrans.Day[i].P4FDate.aniSlideLeftWord->animation);
			
			animation_set_duration(&P4GDTDTrans.Day[i].P4FDate.aniSlideLeftDay->animation, DT_ANI_SLIDE_DURATION);
			animation_set_delay(&P4GDTDTrans.Day[i].P4FDate.aniSlideLeftDay->animation, DT_ANI_SLIDE_DELAY);

			if (i == 3 )
			{
				if (P4GDTDTrans.State.bChangeDay)
				{
					animation_set_handlers(&P4GDTDTrans.Day[i].P4FDate.aniSlideLeftDay->animation, (AnimationHandlers){
					.stopped = (AnimationStoppedHandler)DTSlidingAnimationStopped
					}, context);
				}
				else
				{
					P4GDTDTrans.aniHold = property_animation_create_layer_frame(P4GDTDTrans.lyrP4GDTDTrans,&P4GDTDTrans.fraBackground,&P4GDTDTrans.fraBackground);
					P4GDTDTrans.State.bAniHoldExists = true;
					animation_set_duration( &P4GDTDTrans.aniHold->animation, DT_ANI_HOLD_DURATION); //500
					animation_set_handlers(&P4GDTDTrans.aniHold->animation, (AnimationHandlers){
					.stopped = (AnimationStoppedHandler)DTHoldAnimationStopped
					}, NULL);
					//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "slide stopped left");
					animation_schedule(&P4GDTDTrans.aniHold->animation);
				}
			}
			
			
			animation_schedule(&P4GDTDTrans.Day[i].P4FDate.aniSlideLeftDay->animation);

			P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg = property_animation_create_layer_frame(bitmap_layer_get_layer(P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.bmplyrWeather),&P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.fraWeatherFrame,&P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.fraImgEnd);
			P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.State.bAniSlideLeftImgExists = true;
			animation_set_duration(&P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg->animation, DT_ANI_SLIDE_DURATION);
			animation_set_delay(&P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg->animation, DT_ANI_SLIDE_DELAY);
			animation_schedule(&P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg->animation);
			
			//if (P4GDTDTrans.Day[i].P4FSplitWeather.State.bSplit && P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_TOP.State.bSplit && P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.State.bSplit)
			if (P4GDTDTrans.Day[i].P4FSplitWeather.State.bSplit)
			{
				P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg = property_animation_create_layer_frame(bitmap_layer_get_layer(P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.bmplyrWeather),&P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.fraWeatherFrame,&P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.fraImgEnd);
				P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.State.bAniSlideLeftImgExists = true;
				animation_set_duration(&P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg->animation, DT_ANI_SLIDE_DURATION);
				animation_set_delay(&P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg->animation, DT_ANI_SLIDE_DELAY);				
				animation_schedule(&P4GDTDTrans.Day[i].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg->animation);
			}
		} 
		if (P4GDTDTrans.State.bChangeDay)
		{
			if (strcmp(txtCurrentMonth,txtPreviousMonth) != 0)
			{
				P4GDTDTrans.State.bMonthChanged = true;
				
				P4GDTDTrans.fraCoverPreviousMonth	=	DT_FRA_COVER_PREVIOUS_MONTH;
				P4GDTDTrans.fraPreviousMonth		=	DT_FRA_PREVIOUS_MONTH;
				P4GDTDTrans.fraCurrentMonth			=	DT_FRA_CURRENT_MONTH;
				
				P4GDTDTrans.aniPreviousMonth = property_animation_create_layer_frame(text_layer_get_layer(P4GDTDTrans.txtlyrPreviousMonth),&P4GDTDTrans.fraPreviousMonth,&P4GDTDTrans.fraCoverPreviousMonth);
				P4GDTDTrans.State.bAniPreviousMonthExists = true;
				animation_set_duration(&P4GDTDTrans.aniPreviousMonth->animation, DT_ANI_SLIDE_DURATION);
				animation_set_delay(&P4GDTDTrans.aniPreviousMonth->animation, DT_ANI_SLIDE_DELAY);
				
				P4GDTDTrans.aniCurrentMonth = property_animation_create_layer_frame(text_layer_get_layer(P4GDTDTrans.txtlyrCurrentMonth),&P4GDTDTrans.fraCurrentMonth,&P4GDTDTrans.fraPreviousMonth);
				P4GDTDTrans.State.bAniCurrentMonthExists = true;
				animation_set_duration(&P4GDTDTrans.aniCurrentMonth->animation, DT_ANI_SLIDE_DURATION);
				animation_set_delay(&P4GDTDTrans.aniCurrentMonth->animation, DT_ANI_SLIDE_DELAY);
				
				if (strcmp(txtCurrentYear,txtPreviousYear) != 0)
				{
					P4GDTDTrans.State.bYearChanged = true;
					P4GDTDTrans.fraCoverPreviousYear	=	DT_FRA_COVER_PREVIOUS_YEAR;
					P4GDTDTrans.fraPreviousYear		=	DT_FRA_PREVIOUS_YEAR;
					P4GDTDTrans.fraCurrentYear		=	DT_FRA_CURRENT_YEAR;
					
					P4GDTDTrans.aniPreviousYear = property_animation_create_layer_frame(text_layer_get_layer(P4GDTDTrans.txtlyrPreviousYear),&P4GDTDTrans.fraPreviousYear,&P4GDTDTrans.fraCoverPreviousYear);
					P4GDTDTrans.State.bAniPreviousYearExists = true;
					animation_set_duration(&P4GDTDTrans.aniPreviousYear->animation, DT_ANI_SLIDE_DURATION);
					animation_set_delay(&P4GDTDTrans.aniPreviousYear->animation, DT_ANI_SLIDE_DELAY);
					
					P4GDTDTrans.aniCurrentYear = property_animation_create_layer_frame(text_layer_get_layer(P4GDTDTrans.txtlyrCurrentYear),&P4GDTDTrans.fraCurrentYear,&P4GDTDTrans.fraPreviousYear);
					P4GDTDTrans.State.bAniCurrentYearExists = true;
					animation_set_duration(&P4GDTDTrans.aniCurrentYear->animation, DT_ANI_SLIDE_DURATION);
					animation_set_delay(&P4GDTDTrans.aniCurrentYear->animation, DT_ANI_SLIDE_DELAY);
					
					animation_schedule(&P4GDTDTrans.aniPreviousYear->animation);
					animation_schedule(&P4GDTDTrans.aniCurrentYear->animation);
				}
				
				animation_set_handlers(&P4GDTDTrans.aniPreviousMonth->animation, (AnimationHandlers){
				.stopped = (AnimationStoppedHandler)DTSlidingAnimationStopped
				}, NULL);
				
				animation_schedule(&P4GDTDTrans.aniPreviousMonth->animation);	
				animation_schedule(&P4GDTDTrans.aniCurrentMonth->animation);
			}
		}
		
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "end of opening stop");
		//only set handlers on the last frame.
		/*
		P4GDTDTrans.Day[2].P4FDate.aniSlideLeftWord = property_animation_create_layer_frame(text_layer_get_layer(P4GDTDTrans.Day[2].P4FDate.txtlyrDateWord),&P4GDTDTrans.Day[2].P4FDate.fraWordFrame,&P4GDTDTrans.Day[2].P4FDate.fraWordEnd);
		P4GDTDTrans.Day[2].P4FDate.State.bAniSlideLeftWordExists = true;
		
		P4GDTDTrans.Day[2].P4FDate.aniSlideLeftDay = property_animation_create_layer_frame(text_layer_get_layer(P4GDTDTrans.Day[2].P4FDate.txtlyrDate),&P4GDTDTrans.Day[2].P4FDate.fraDateFrame, &P4GDTDTrans.Day[2].P4FDate.fraDateEnd);
		P4GDTDTrans.Day[2].P4FDate.State.bAniSlideLeftDayExists = true;
		
		animation_set_duration(&P4GDTDTrans.Day[2].P4FDate.aniSlideLeftWord, DT_ANI_SLIDE_DURATION);
		animation_set_delay(&P4GDTDTrans.Day[2].P4FDate.aniSlideLeftWord, DT_ANI_SLIDE_DELAY);
		animation_schedule(&P4GDTDTrans.Day[2].P4FDate.aniSlideLeftWord);
		
		animation_set_duration(&P4GDTDTrans.Day[2].P4FDate.aniSlideLeftDay, DT_ANI_SLIDE_DURATION);
		animation_set_delay(&P4GDTDTrans.Day[2].P4FDate.aniSlideLeftDay, DT_ANI_SLIDE_DELAY);
		animation_schedule(&P4GDTDTrans.Day[2].P4FDate.aniSlideLeftDay);
		*/
		/*
		P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg = property_animation_create_layer_frame(bitmap_layer_get_layer(P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.bmplyrWeather),&P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.fraWeatherFrame,&P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.fraImgEnd);
		P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.State.bAniSlideLeftImgExists = true;
			
		animation_set_duration(&P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg, DT_ANI_SLIDE_DURATION);
		animation_set_delay(&P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg, DT_ANI_SLIDE_DELAY);
		animation_schedule(&P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg);
		if (P4GDTDTrans.Day[2].P4FSplitWeather.State.bSplit)
		{
			P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg = property_animation_create_layer_frame(bitmap_layer_get_layer(P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.bmplyrWeather),&P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.fraWeatherFrame,&P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.fraImgEnd);
			P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.State.bAniSlideLeftImgExists = true;
				
			animation_set_duration(&P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg, DT_ANI_SLIDE_DURATION);
			animation_set_delay(&P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg, DT_ANI_SLIDE_DELAY);
			
			animation_schedule(&P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg);
		}
		
		P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg = property_animation_create_layer_frame(bitmap_layer_get_layer(P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.bmplyrWeather),&P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.fraWeatherFrame,&P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.fraImgEnd);
		P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.State.bAniSlideLeftImgExists = true;
			
		animation_set_duration(&P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg, DT_ANI_SLIDE_DURATION);
		animation_set_delay(&P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg, DT_ANI_SLIDE_DELAY);
		animation_schedule(&P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg);
		
		
		// animation_set_handlers(&P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg, (AnimationHandlers){
			//.stopped = (AnimationStoppedHandler)DTSlidingAnimationStopped
		//}, context);
		
		animation_schedule(&P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg);
		
		if (P4GDTDTrans.Day[3].P4FSplitWeather.State.bSplit)
		{
			P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg = property_animation_create_layer_frame(bitmap_layer_get_layer(P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.bmplyrWeather),&P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.fraWeatherFrame,&P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.fraImgEnd);
			P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.State.bAniSlideLeftImgExists = true;
				
			animation_set_duration(&P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg, DT_ANI_SLIDE_DURATION);
			animation_set_delay(&P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg, DT_ANI_SLIDE_DELAY);
			
			animation_schedule(&P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg);
		}
		*/
	}
}

//QT:DTSTART QT:DAYTRANSITION QT:DAY QT:DTSTART
void StartDTDTrans()
{	
	P4GDTDTrans.State.bInvertExists = true;
	P4GDTDTrans.invlyrInvert = inverter_layer_create(P4GDTDTrans.fraStart);
	layer_add_child(P4GDTDTrans.lyrP4GDTDTrans,inverter_layer_get_layer(P4GDTDTrans.invlyrInvert));		
			
	//opening bar animation
	//P4GDTDTrans.aniOpening = property_animation_create_layer_frame(bitmap_layer_get_layer(P4GDTDTrans.bmplyrDayTrans),&P4GDTDTrans.fraStart,&P4GDTDTrans.fraEnd);
	P4GDTDTrans.aniOpening = property_animation_create_layer_frame(inverter_layer_get_layer(P4GDTDTrans.invlyrInvert),&P4GDTDTrans.fraStart,&P4GDTDTrans.fraEnd);
	P4GDTDTrans.State.bAniOpeningExists = true;
	animation_set_duration(&P4GDTDTrans.aniOpening->animation, DT_ANI_OPEN_DURATION);
	animation_set_delay(&P4GDTDTrans.aniOpening->animation, DT_ANI_OPEN_DELAY);
	animation_set_handlers(&P4GDTDTrans.aniOpening->animation, (AnimationHandlers){
		.stopped = (AnimationStoppedHandler)DTOpeningAnimationStopped
		//.stopped = (AnimationStoppedHandler)DTSlidingAnimationStopped	
	}, NULL);
	//.stopped = (AnimationStoppedHandler)DTSlidingAnimationStopped	
	//.stopped = (AnimationStoppedHandler)DTHoldAnimationStopped	
	animation_schedule(&P4GDTDTrans.aniOpening->animation);

}

//void DTDTRANS_INIT(P4FFONTS inputP4FFonts, Window* window)
void DTDTRANS_INIT(bool bInputChangeDay, Window* window)
{
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "DTDT Trans Init");
	//P4FDTDTFonts	=	inputP4FFonts;
	if (!P4GDTDTrans.State.bFontsInitialized)
	{
		InitAllFonts(&P4GDTDTrans.P4FDTDTRANSFonts);
		P4GDTDTrans.State.bFontsInitialized = true;
	}
	
	P4GDTDTrans.State.bChangeDay = bInputChangeDay;
	P4GDTDTrans.fraStart = DT_FRA_START;
	P4GDTDTrans.fraEnd = DT_FRA_END;
	P4GDTDTrans.fraPreviousMonth = DT_FRA_PREVIOUS_MONTH;	
	P4GDTDTrans.fraCurrentMonth = DT_FRA_CURRENT_MONTH;	
	P4GDTDTrans.fraPreviousYear = DT_FRA_PREVIOUS_YEAR;
	P4GDTDTrans.fraCurrentYear = DT_FRA_CURRENT_YEAR;
	P4GDTDTrans.fraBackground = WATCH_FRA_BACKGROUND;
	
	
	P4GDTDTrans.lyrP4GDTDTrans = layer_create(P4GDTDTrans.fraBackground);
	layer_add_child(window_get_root_layer(window), P4GDTDTrans.lyrP4GDTDTrans);
	
		
	P4GDTDTrans.State.bAniOpeningExists = false;
	P4GDTDTrans.State.bAniClosingLeftExists = false;	
	P4GDTDTrans.State.bAniClosingRightExists = false;
	P4GDTDTrans.State.bAniHoldExists = false;	
	P4GDTDTrans.State.bAniPreviousMonthExists = false;
	P4GDTDTrans.State.bAniPreviousYearExists = false;
	P4GDTDTrans.State.bAniCurrentMonthExists = false;
	P4GDTDTrans.State.bAniCurrentYearExists = false;
	P4GDTDTrans.State.bPreviousMonthExists = false;
	P4GDTDTrans.State.bPreviousYearExists = false;
	P4GDTDTrans.State.bCurrentMonthExists = false;
	P4GDTDTrans.State.bCurrentYearExists = false;
	P4GDTDTrans.State.bInvertExists = false;
	P4GDTDTrans.State.bClosingLeftExists = false;
	P4GDTDTrans.State.bClosingRightExists = false;
	
	P4GDTDTrans.State.bDTDTransitioning = true;
	P4GDTDTrans.State.bExists = true;
	P4GDTDTrans.State.bPastPointofNoReturn = false;
	P4GDTDTrans.State.bInitialized = true;
	/*
	if (bUseInvertedColors)
	{
		P4GDTDTrans.invlyrInvertDTDT = inverter_layer_create(P4GDTDTrans.fraBackground);
		layer_add_child(window_get_root_layer(window),inverter_layer_get_layer(P4GDTDTrans.invlyrInvertDTDT));		
		P4GDTDTrans.State.bDTDTInvertExists = true;
	}
	else
	{
	*/
		P4GDTDTrans.State.bDTDTInvertExists = false;
	//}
	
	StartDTDTrans();
	//RequestWeather();
}


void RemoveAndDeIntDTDTRANSDAY()
{	
		
	if (P4GDTDTrans.Day[0].State.bDayExists)	
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "remove day 0");
		RemoveAndDeIntAllDate(&P4GDTDTrans.Day[0].P4FDate);
		P4FDATE_DEINIT(&P4GDTDTrans.Day[0].P4FDate);
		if (P4GDTDTrans.Day[0].P4FSplitWeather.P4FSplitWeather_TOP.State.bAniSlideLeftImgExists)
		{
			if (animation_is_scheduled(&P4GDTDTrans.Day[0].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg->animation))
			{
				animation_unschedule(&P4GDTDTrans.Day[0].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg->animation);
			}
			if (animation_is_scheduled(&P4GDTDTrans.Day[0].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg->animation))
			{
				animation_unschedule(&P4GDTDTrans.Day[0].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg->animation);
			}
		}
		RemoveAndDeIntSplitWeather(&P4GDTDTrans.Day[0].P4FSplitWeather);
		P4FSPLITWEATHER_DEINIT(&P4GDTDTrans.Day[0].P4FSplitWeather);
		P4GDTDTrans.Day[0].State.bDayExists = false;
	}
		
	if (P4GDTDTrans.Day[2].State.bDayExists)
	{
		//all of the following end
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "remove day 2");
		//really day 1
		RemoveAndDeIntAllDate(&P4GDTDTrans.Day[2].P4FDate);
		P4FDATE_DEINIT(&P4GDTDTrans.Day[2].P4FDate);
		if (P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.State.bAniSlideLeftImgExists)
		{
			if (animation_is_scheduled(&P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg->animation))
			{
				animation_unschedule(&P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg->animation);
			}
			if (animation_is_scheduled(&P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg->animation))
			{
				animation_unschedule(&P4GDTDTrans.Day[2].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg->animation);
			}
		}
		RemoveAndDeIntSplitWeather(&P4GDTDTrans.Day[2].P4FSplitWeather);
		P4FSPLITWEATHER_DEINIT(&P4GDTDTrans.Day[2].P4FSplitWeather);
		P4GDTDTrans.Day[2].State.bDayExists = false;
	}
	
	if (P4GDTDTrans.Day[3].State.bDayExists)	
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "remove day 3");
		RemoveAndDeIntAllDate(&P4GDTDTrans.Day[3].P4FDate);
		P4FDATE_DEINIT(&P4GDTDTrans.Day[3].P4FDate);
		if (P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.State.bAniSlideLeftImgExists)
		{
			if (animation_is_scheduled(&P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg->animation))
			{
				animation_unschedule(&P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg->animation);
			}
			if (animation_is_scheduled(&P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg->animation))
			{
				animation_unschedule(&P4GDTDTrans.Day[3].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg->animation);
			}
		}
		RemoveAndDeIntSplitWeather(&P4GDTDTrans.Day[3].P4FSplitWeather);
		P4FSPLITWEATHER_DEINIT(&P4GDTDTrans.Day[3].P4FSplitWeather);
		P4GDTDTrans.Day[3].State.bDayExists = false;
	}
}


void RemoveAndDeIntAllDTDT()
{
	if (P4GDTDTrans.State.bExists && P4GDTDTrans.State.bInitialized)	
	{
		RemoveAndDeIntDTDTRANSDAY();
		animation_unschedule_all();
		
		if (P4GDTDTrans.State.bAniOpeningExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Opening");
			property_animation_destroy(P4GDTDTrans.aniOpening);
			P4GDTDTrans.State.bAniOpeningExists = false;
		}
		
		if (P4GDTDTrans.State.bAniClosingLeftExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani closing");
			property_animation_destroy(P4GDTDTrans.aniClosingLeft);
			P4GDTDTrans.State.bAniClosingLeftExists = false;
		}
		
		if (P4GDTDTrans.State.bAniClosingRightExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Closing");
			property_animation_destroy(P4GDTDTrans.aniClosingRight);
			P4GDTDTrans.State.bAniClosingRightExists = false;
		}
		
		if (P4GDTDTrans.State.bAniHoldExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Hold");
			property_animation_destroy(P4GDTDTrans.aniHold);
			P4GDTDTrans.State.bAniHoldExists = false;
		}
		
		if (P4GDTDTrans.State.bAniPreviousMonthExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Previous M");
			property_animation_destroy(P4GDTDTrans.aniPreviousMonth);
			P4GDTDTrans.State.bAniPreviousMonthExists = false;
		}
		
		if (P4GDTDTrans.State.bAniPreviousYearExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Prev Year");
			property_animation_destroy(P4GDTDTrans.aniPreviousYear);
			P4GDTDTrans.State.bAniPreviousYearExists = false;
		}
		
		if (P4GDTDTrans.State.bAniCurrentMonthExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Month");
			property_animation_destroy(P4GDTDTrans.aniCurrentMonth);
			P4GDTDTrans.State.bAniCurrentMonthExists = false;
		}
		
		if (P4GDTDTrans.State.bAniCurrentYearExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Ani Year");
			property_animation_destroy(P4GDTDTrans.aniCurrentYear);
			P4GDTDTrans.State.bAniCurrentYearExists = false;
		}
		
		if (P4GDTDTrans.State.bPreviousMonthExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT P Month");
			layer_remove_from_parent(text_layer_get_layer(P4GDTDTrans.txtlyrPreviousMonth));
			text_layer_destroy(P4GDTDTrans.txtlyrPreviousMonth);		
			P4GDTDTrans.State.bPreviousMonthExists = false;
		}
			
		if (P4GDTDTrans.State.bPreviousYearExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Prev Y");
			layer_remove_from_parent(text_layer_get_layer(P4GDTDTrans.txtlyrPreviousYear));
			text_layer_destroy(P4GDTDTrans.txtlyrPreviousYear);		
			P4GDTDTrans.State.bPreviousYearExists = false;
		}
		
		if (P4GDTDTrans.State.bCurrentMonthExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Cur");
			layer_remove_from_parent(text_layer_get_layer(P4GDTDTrans.txtlyrCurrentMonth));
			text_layer_destroy(P4GDTDTrans.txtlyrCurrentMonth);		
			P4GDTDTrans.State.bCurrentMonthExists = false;
		}
		
		if (P4GDTDTrans.State.bCurrentYearExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT C Y");
			layer_remove_from_parent(text_layer_get_layer(P4GDTDTrans.txtlyrCurrentYear));
			text_layer_destroy(P4GDTDTrans.txtlyrCurrentYear);	
			P4GDTDTrans.State.bCurrentYearExists = false;
		}
		
		if (P4GDTDTrans.State.bInvertExists)
		{	
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Inv");
			layer_remove_from_parent(inverter_layer_get_layer(P4GDTDTrans.invlyrInvert));
			inverter_layer_destroy(P4GDTDTrans.invlyrInvert);		
			P4GDTDTrans.State.bInvertExists = false;
		}
		
		if (P4GDTDTrans.State.bClosingLeftExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Close L");
			layer_remove_from_parent(bitmap_layer_get_layer(P4GDTDTrans.bmplyrClosingLeft));
			bitmap_layer_destroy(P4GDTDTrans.bmplyrClosingLeft);		
			P4GDTDTrans.State.bClosingLeftExists = false;
		}
		
		if (P4GDTDTrans.State.bClosingRightExists)
		{
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Delete DTDT Close R");
			layer_remove_from_parent(bitmap_layer_get_layer(P4GDTDTrans.bmplyrClosingRight));
			bitmap_layer_destroy(P4GDTDTrans.bmplyrClosingRight);		
			P4GDTDTrans.State.bClosingRightExists = false;
		}
	
		
		if (P4GDTDTrans.Day[1].State.bDayExists)
		{
			//all of the following end
			//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "remove day 1");
			//really day 1
			RemoveAndDeIntAllDate(&P4GDTDTrans.Day[1].P4FDate);
			P4FDATE_DEINIT(&P4GDTDTrans.Day[1].P4FDate);
			if (P4GDTDTrans.Day[1].P4FSplitWeather.P4FSplitWeather_TOP.State.bAniSlideLeftImgExists)
			{
				if (animation_is_scheduled(&P4GDTDTrans.Day[1].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg->animation))
				{
					animation_unschedule(&P4GDTDTrans.Day[1].P4FSplitWeather.P4FSplitWeather_TOP.aniSlideLeftImg->animation);
				}
				if (animation_is_scheduled(&P4GDTDTrans.Day[1].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg->animation))
				{
					animation_unschedule(&P4GDTDTrans.Day[1].P4FSplitWeather.P4FSplitWeather_BOTTOM.aniSlideLeftImg->animation);
				}
			}
			RemoveAndDeIntSplitWeather(&P4GDTDTrans.Day[1].P4FSplitWeather);
			P4FSPLITWEATHER_DEINIT(&P4GDTDTrans.Day[1].P4FSplitWeather);
			P4GDTDTrans.Day[1].State.bDayExists = false;
		}
		/*  
		if (P4GDTDTrans.State.bFontsInitialized)
		{
			RemoveAndDeIntAllFonts(&P4GDTDTrans.P4FDTDTRANSFonts);
			P4FFONTS_DEINIT_ALL(&P4GDTDTrans.P4FDTDTRANSFonts);	
			P4GDTDTrans.State.bFontsInitialized = false;
		}
		*/
		
		if (P4GDTDTrans.State.bDTDTInvertExists)
		{
			layer_remove_from_parent(inverter_layer_get_layer(P4GDTDTrans.invlyrInvertDTDT));
			inverter_layer_destroy(P4GDTDTrans.invlyrInvertDTDT);
			P4GDTDTrans.State.bDTDTInvertExists = false;
		}
		
			

		layer_remove_from_parent(P4GDTDTrans.lyrP4GDTDTrans);
		layer_destroy(P4GDTDTrans.lyrP4GDTDTrans);
		P4GDTDTRANS_DEINIT();
		//if (P4GDTDTrans.State.bChangeDay)
		//{
			//CheckPersistDays();
		//}
		
		P4GDTDTrans.State.bChangeDay = false;
		if (!P4GDTDTrans.State.bClosing)
		{
			window_load();//in P4G
		}
		//note that we do not alter the past point of no return, this is so that they aren't created when they shouldn't be.
	}
}

void P4GDTDTRANS_DEINIT()
{
	P4GDTDTrans.State.bExists = false;
	P4GDTDTrans.State.bDTDTransitioning = false;
	P4GDTDTrans.State.bInitialized = false;
	P4GDTDTrans.State.bMonthChanged = false;
	P4GDTDTrans.State.bYearChanged = false;
}
